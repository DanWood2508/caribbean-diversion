package com.elevenfiftyfive.caribbeandiversion.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;

public class MiniGamePirate {
	// The position and velocity of the pirate stored as three-dimensional vectors
	private Vector3 position;
	private Vector3 velocity;
	// The point at which the pirate should stop based on the direction of movement
	private int stopPoint;
	// The direction the pirate is moving in
	private String direction;
	// Whether the pirate is moving or not, to ignore subsequent inputs
	private boolean inMotion;
	
	// The SpriteBatch for right direction motion
	private SpriteBatch rightBatch;
	// The Spritesheet for right direction motion
	private Texture pirateRight;
	// The Spritesheet split into individual frames
	private TextureRegion[] rightAnimationFrames;
	// The animation for the right motion
	private Animation<TextureRegion> rightAnimation;
	// Elapsed time moving right to monitor repeating the animation
	private float rightElapsedTime;
	
	// The SpriteBatch for the left direction motion
	private SpriteBatch leftBatch;
	// The Spritesheet for right direction motion
	private Texture pirateLeft;
	// The Spritesheet split into individual frames
	private TextureRegion[] leftAnimationFrames;
	// The animation for the left motion
	private Animation<TextureRegion> leftAnimation;
	// Elapsed time moving left to monitor repeating the animation
	private float leftElapsedTime;
	
	public MiniGamePirate(int x, int y) {
		position = new Vector3(x, y, 0);
		velocity = new Vector3(0, 0, 0);
		stopPoint = 0;
		inMotion = false;
		
		rightBatch = new SpriteBatch();
		// Load the spritesheet
		pirateRight = new Texture("minigame/pirateRight.png");
		// Split by the dimensions of one frame
		TextureRegion[][] rightTmpFrames = TextureRegion.split(pirateRight, 15, 29);
		// Save these into an array
		rightAnimationFrames = new TextureRegion[6];
		int rightIndex = 0;
		for(int i = 0; i < 1; i++) {
		    for(int j = 0; j < 6; j++) {  
		       rightAnimationFrames[rightIndex++] = rightTmpFrames[i][j];
		   }
		}
		rightAnimation = new Animation<TextureRegion>(1/16f, rightAnimationFrames);
	
		leftBatch = new SpriteBatch();
		// Load the spritesheet
		pirateLeft = new Texture("minigame/pirateLeft.png");
		// Split by the dimensions of one frame
		TextureRegion[][] leftTmpFrames = TextureRegion.split(pirateLeft, 15, 29);
		// Save these into an array
		leftAnimationFrames = new TextureRegion[6];
		int leftIndex = 0;
		for(int i = 0; i < 1; i++) {
		    for(int j = 0; j < 6; j++) {  
		       leftAnimationFrames[leftIndex++] = leftTmpFrames [i][j];
		   }
		}
		leftAnimation = new Animation<TextureRegion>(1/16f, leftAnimationFrames);
	}
	
	/**
	 * Control the movement of the pirate, move the pirate in the specified direction of key-press and control animation
	 */
	public void render() {	
		// If the pirate is in motion, either north or eastbound, draw the right hand animation
		if (inMotion && (direction == "north" || direction == "east")) {
			rightBatch.begin();
	    	rightElapsedTime += Gdx.graphics.getDeltaTime();
	    	rightBatch.setColor(1.0f, 1.0f, 1.0f, 1.0f);
	    	rightBatch.draw(rightAnimation.getKeyFrame(rightElapsedTime, true), position.x, position.y, 30, 58);
	    	rightBatch.end();
	    }
		// If the pirate is in motion, either south or westbound, draw the left hand animation
		else if (inMotion && (direction == "west" || direction == "south")) {
			leftBatch.begin();
	    	leftElapsedTime += Gdx.graphics.getDeltaTime();
	    	leftBatch.setColor(1.0f, 1.0f, 1.0f, 1.0f);
	    	leftBatch.draw(leftAnimation.getKeyFrame(leftElapsedTime, true), position.x, position.y, 30, 58);
	    	leftBatch.end();
		}
		else {
			rightBatch.begin();
			rightBatch.draw(rightAnimation.getKeyFrame(0.0f,true),position.x, position.y, 30, 58);
			rightBatch.end();
		}
	}
	
	
	/** 
	 * Continually update the velocity and movement of the pirate
	 * 
	 * @param dt The rate at which the application refreshes
	 */
	public void update(float dt) {
		// If the pirate is moving past its stop point (current position + 1) then stop moving to allow direction to be changed, only moving in small chunks
		if((direction == "west" && position.x < stopPoint) || (direction == "east" && position.x > stopPoint) || (direction == "north" && position.y > stopPoint) || (direction == "south" && position.y < stopPoint)) {
			stopMoving();
		}
		else {
			// Move the pirate across the screen
			velocity.scl(dt);
			position.add(velocity.x, velocity.y, 0);
			velocity.scl(1/dt);
		}
		
	}
	
	
	/** 
	 * Determine the position of the pirate
	 * 
	 * @return Vector3
	 */
	public Vector3 getPosition() {
		// Get the current position of the Pirate in a 3-dimensional vector
		return position;
	}
	
	/**
	 * Get the Pirate to stop moving by resetting x and y components of velocity and setting inMotion to false
	 */
	public void stopMoving() {
		// Stop all horizontal and vertical motion, reset the stopping point to allow this to be changed again
		velocity.x = 0;
		velocity.y = 0;
		stopPoint = 0;
		inMotion = false;
	}
	
	
	/** 
	 * Move the pirate up and stop after one pixel of movement
	 * 
	 * @param currentPos The starting position of the pirate before moving up
	 */
	public void moveUp(int currentPos) {
		// Allow this to move up one position only, increase y at a rate of 75
		this.stopPoint = currentPos + 1;
		inMotion = true;
		direction = "north";
		velocity.y = 75;
		velocity.x = 0;
	}
	
	
	/** 
	 * Move the pirate down and stop after one pixel of movement
	 * 
	 * @param currentPos The starting position of the pirate before moving down
	 */
	public void moveDown(int currentPos) {
		// Allow this to move down one position only, decrease y at a rate of 75
		this.stopPoint = currentPos - 1;
		inMotion = true;
		direction = "south";
		velocity.y = -75;
		velocity.x = 0;
	}
	
	
	/**
	 * Move the pirate left and stop after one pixel of movement
	 *  
	 * @param currentPos The starting position of the pirate before moving left
	 */
	public void moveLeft(int currentPos) {
		// Allow this to move left one position only, decrease x at a rate of 75
		this.stopPoint = currentPos - 1;
		inMotion = true;
		direction = "west";
		velocity.x = -75;
		velocity.y = 0;
	}
	
	
	/** 
	 * Move the pirate right and stop after one pixel of movement
	 * 
	 * @param currentPos The starting position of the pirate before moving right
	 */
	public void moveRight(int currentPos) {
		// Allow this to move right one position only, increase x at a rate of 75
		this.stopPoint = currentPos + 1;
		inMotion = true;
		direction = "east";
		velocity.x = 75;
		velocity.y = 0;
	}
	 
	/**
	 * Dispose of all unneeded resources to save application memory
	 */
	public void dispose() {
		// Dispose the left and right batch as they are no longer needed, this frees up application memory
		leftBatch.dispose();
		rightBatch.dispose();
	}
}
