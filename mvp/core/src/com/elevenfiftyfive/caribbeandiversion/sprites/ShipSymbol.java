package com.elevenfiftyfive.caribbeandiversion.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class ShipSymbol {
	// This is a three dimensional position vector of the Ship
	private Vector3 position;
	// This is a three dimensional velocity vector of the Ship
	private Vector3 velocity;
	// These are the bounds in which the ship can occupy
	private Rectangle bounds;
	// This is the texture of the ship
	private Texture symbol;
	// This is the animation class for the Ship
	private ShipAnimation boatAnimation;
	// This is the point at which the ship should stop to stop it going out of bounds
	private int stopPoint;
	
	public ShipSymbol(int x, int y) {
		position = new Vector3(x, y, 0);
		velocity = new Vector3(0, 0, 0);
		symbol = new Texture ("boat map symbol.png");
		boatAnimation = new ShipAnimation(new TextureRegion(symbol), 1, 0f);
		bounds = new Rectangle(x, y, symbol.getWidth() / 12, symbol.getHeight());
		stopPoint = 0;
	}
	
	
	/** 
	 * Update the velocity and motion constantly
	 * 
	 * @param dt The time frame within which to update
	 */
	public void update(float dt) {
		// If the x position is more than stop point, then stop all motion to avoid going out of bounds
		if(position.x > stopPoint) {
			velocity.x = 0;
			velocity.y = 0;
		}
		else {
			// Continue with movement
			velocity.scl(dt);
			position.add(velocity.x, velocity.y, 0);
			velocity.scl(1/dt);
		}
		// Animate the motion of the boat
		boatAnimation.update(dt);
		bounds.setPosition(position.x, position.y);
	}
	
	
	/** 
	 * Determine the position of the ship
	 * 
	 * @return Vector3
	 */
	// Determine the position of the boat with a three-dimensional vector
	public Vector3 getPosition() {
		return position;
	}
	
	
	/** 
	 * Determine the texture of the ship
	 * 
	 * @return TextureRegion
	 */
	// Get the texture to draw onto the Stage
	public TextureRegion getTexture() {
		return boatAnimation.getFrame();
	}
	
	
	/** 
	 * Move the ship to the hard island, stop at the designated stop point
	 * 
	 * @param stopPoint The x value at which to stop
	 */
	// Direction at which ship is moving, values to stop at correct point at island
	public void moveTopRight(int stopPoint) {
		this.stopPoint = stopPoint;
		velocity.y = 58;
		velocity.x = 250;
	}
	
	
	/** 
	 * Move the ship to the easy island, stop at the designated stop point
	 * 
	 * @param stopPoint The x value at which to stop
	 */
	// Direction at which ship is moving, values to stop at correct point at island
	public void moveBottomRight(int stopPoint) {
		this.stopPoint = stopPoint;
		velocity.y = -75;
		velocity.x = 250;
	}
	
	/** 
	 * Move the island to the medium island, stop at the designated stop point. Only x component of motion
	 * 
	 * @param stop The x value at which to stop
	 */
	// Direction at which ship is moving, values to stop at correct point at island
	public void moveRight(int stop) {
		this.stopPoint = stop;
		velocity.x = 250;
	}
	
	
	/** 
	 * Determine the bounds in which the ship can move
	 * 
	 * @return Rectangle
	 */
	// To ensure that the ship symbol does not go out of bounds
	public Rectangle getBounds() {
		return bounds;
	}
	
	/**
	 * Stop drawing resources which are no longer needed to save on application memory
	 */
	// Stop drawing the symbol when it is not needed to efficiently use application memory
	public void dispose() {
		symbol.dispose();
	}
	
}
