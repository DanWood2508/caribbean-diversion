package com.elevenfiftyfive.caribbeandiversion;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class MinigameAudio implements ApplicationListener{
	// This will create the variables which will hold instances of the mini game background music and the sound effect
	private Music minigameAudio;
	private Music minigameSoundEffect;
	
	public MinigameAudio() {
		create();
	}
	
	@Override
	public void create() {
		// Import both the audio and sound effect using the LibGDX import utility
		minigameAudio = Gdx.audio.newMusic(Gdx.files.internal("minigame/minigame-pirate-music.mp3"));
		minigameSoundEffect = Gdx.audio.newMusic(Gdx.files.internal("minigame/select.mp3"));
		// The volume of the sound effect will be higher so that this can be differentiated against the background audio
		minigameSoundEffect.setVolume(2.0f);
		// The background audio should be looping in case the user remains on the mini game for an extended period of time
		minigameAudio.setLooping(true);
		minigameAudio.setVolume(0.15f);
		// Should play immediately
		minigameAudio.play();
	}

	
	/** 
	 * Resize the Window
	 * 
	 * @param width Width of the Window
	 * @param height Height of the Window
	 */
	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void render() {
		// If the render() method is called, the sound effect to signify collecting a tool will be called
		minigameSoundEffect.play();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		// If the mini game audio is no longer needed, both tracks will be disposed of to reduce application memory usage.
		minigameAudio.dispose();
		minigameSoundEffect.dispose();
	}

}
