package com.elevenfiftyfive.caribbeandiversion.states;

import java.util.Stack;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameStateManager {
	// This is a stack of States, to determine which is being used
	private Stack<State> states;
	
	public GameStateManager() {
		// This initialises the stack of States
		states = new Stack<State>();
	}
	
	/** 
	 * Push a state to the stack, so that this takes precedence over those below
	 * 
	 * @param state The state to push to the stack
	 */
	public void push(State state) {
		// Add to the stack
		states.push(state);
	}

	/**
	 * Remove the top-most state from the stack, this will give precedence to the state below, if present
	 */
	public void pop() {
		// Remove from the stack
		states.pop();
	}
	
	
	/** 
	 * This pops the state from the stock of the stack, but adds another to the top to take precedence
	 * 
	 * @param state The state to swap in
	 */
	public void set(State state) {
		// Remove from the top State from the stack and push another into its place
		states.pop();
		states.push(state);
	}
	
	
	/** 
	 * Constantly update the topmost state without removing from the stack
	 * 
	 * @param dt The time period at which the application updates
	 */
	public void update(float dt) {
		// Update the item at the top of the stack, none below
		states.peek().update(dt);
	}
	
	/** 
	 * Render the topmost state without removing from the stack
	 * 
	 * @param sb The SpriteBatch to render in this method
	 */
	public void render(SpriteBatch sb) {
		// Render the item at the top of the stack, none below
		states.peek().render(sb);
	}
}
