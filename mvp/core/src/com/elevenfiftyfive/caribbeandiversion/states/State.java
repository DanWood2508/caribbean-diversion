package com.elevenfiftyfive.caribbeandiversion.states;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

public abstract class State {
	protected OrthographicCamera cam;
	protected Vector3 mouse;
	protected GameStateManager gsm;
	
	protected State(GameStateManager gsm) {
		// Each State has a GameStateManager, camera and mouse
		this.gsm = gsm;
		cam = new OrthographicCamera();
		mouse = new Vector3();
	}
	
	// These are methods that all States have, but are implemented differently for each state
	protected abstract void handleInput();
	public abstract void update(float dt);
	public abstract void render(SpriteBatch sb);
	public abstract void dispose();
	
	

}
