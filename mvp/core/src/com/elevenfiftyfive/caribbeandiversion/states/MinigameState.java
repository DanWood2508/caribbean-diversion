package com.elevenfiftyfive.caribbeandiversion.states;

import java.util.Random;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.elevenfiftyfive.caribbeandiversion.MinigameAudio;
import com.elevenfiftyfive.caribbeandiversion.caribbeandiversion;
import com.elevenfiftyfive.caribbeandiversion.model.PlayerShip;
import com.elevenfiftyfive.caribbeandiversion.sprites.MiniGamePirate;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;

public class MinigameState extends State {
	// This is the Pirate Sprite which controls its own animation and movement
	private MiniGamePirate pirate;
	// This island background
	private Texture island;
	// The compass symbol which generates in the game
	private Texture compass;
	// The compass symbol which sits in the bag to show not filled
	private Texture compassBW;
	// The key symbol which generates in the game
	private Texture key;
	// The key symbol which sits in the bag to show not filled
	private Texture keyBW;
	// The chest symbol which generates in the game
	private Texture treasureChest;
	// The chest symbol which sits in the bag to show not filled
	private Texture treasureChestBW;
	// The wormhole symbol
	private Texture wormhole;
	// The bag slot symbol
	private Texture bag;
	
	// X and Y coordinates of compass
	private int compassX;
	private int compassY;
	// To show if compass collected or not
	private boolean compassCollected;
	// X and Y coordinates of key
	private int keyX;
	private int keyY;
	// To show if key collected or not
	private boolean keyCollected;
	// X and Y coordinates of chest
	private int chestX;
	private int chestY;
	// To show if chest collected or not
	private boolean chestCollected;
	// X and Y coordinates of wormhole
	private int wormholeX;
	private int wormholeY;
	// This is the player instance so that utilities can be accessed
	private PlayerShip player;
	
	private MinigameAudio audio;

	protected MinigameState(GameStateManager gsm, PlayerShip player) {
		super(gsm);
		// Generate the Textures, Sprites and Audio
		island = new Texture("minigame/island.png");
		compass = new Texture("minigame/compass.png");
		compassBW = new Texture("minigame/compassBW.png");
		key = new Texture("minigame/key.png");
		keyBW = new Texture("minigame/keyBW.png");
		treasureChest = new Texture("minigame/treasure chest.png");
		treasureChestBW = new Texture("minigame/treasureBW.png");
		wormhole = new Texture("minigame/teleporter.png");
		bag = new Texture("minigame/bag.png");
		pirate = new MiniGamePirate(110, 350);
		audio = new MinigameAudio();
		this.player = player;
		
		// Generate random numbers for positions of items to find, bounds include width of texture for right hand and top side
		Random r = new Random();
		compassX = r.nextInt((1100-107) + 1) + 107;
		compassY = r.nextInt((585-90) + 1) + 100;
		compassCollected = false;
		
		keyX = r.nextInt((1100-107) + 1) + 107;
		keyY = r.nextInt((585-90) + 1) + 100;
		keyCollected = false;
		
		chestX = r.nextInt((1100-105) + 1) + 107;
		chestY = r.nextInt((585-90) + 1) + 100;
		chestCollected = false;
		
		wormholeX = r.nextInt((1100-107) + 1) + 107;
		wormholeY = r.nextInt((585-90) + 1) + 100;
		
	}
	
	/**
	 * Handle the input pressed, keys determine movement of the Pirate Sprite
	 */
	@Override
	protected void handleInput() { 
		// If the W or UP key is pressed, then move the Pirate up, ensuring this remains within the bounds of the island
		if((Gdx.input.isKeyPressed(Input.Keys.W)) || (Gdx.input.isKeyPressed(Input.Keys.UP))) {
			if (pirate.getPosition().y - 10 < 585) {
				pirate.moveUp(Math.round(pirate.getPosition().y));
			}
		}
		// If the S or DOWN key is pressed, then move the pirate down, ensuring this remains within the bounds of the island
		else if((Gdx.input.isKeyPressed(Input.Keys.S)) || (Gdx.input.isKeyPressed(Input.Keys.DOWN))) {
			if (pirate.getPosition().y + 10 > 90) {
				pirate.moveDown(Math.round(pirate.getPosition().y));
			}
		} 
		// If the D or RIGHT key is pressed, then move the pirate right, ensuring this remains within the bounds of the island
		else if((Gdx.input.isKeyPressed(Input.Keys.D)) || (Gdx.input.isKeyPressed(Input.Keys.RIGHT))) {
			if (pirate.getPosition().x + 10 < 1100) {
				pirate.moveRight(Math.round(pirate.getPosition().x));
			}
		}
		// If the A or LEFT key is pressed, then move the pirate left, ensuring this remains within the bounds of the island
		else if((Gdx.input.isKeyPressed(Input.Keys.A)) || (Gdx.input.isKeyPressed(Input.Keys.LEFT))) {
			if (pirate.getPosition().x - 10 > 107) {
				pirate.moveLeft(Math.round(pirate.getPosition().x));
			}
		}	
	}

	
	/** 
	 * Constantly check for updates and animate the pirate, also constantly check whether sprites have collided
	 * 
	 * @param dt The time period at which the application refreshes itself
	 */
	@Override
	public void update(float dt) {
		handleInput();
		cam.update();
		pirate.update(dt);
		
		// If the compass is not collected yet but the pirate is within its position, then mark as collected and play the audio
		if (!compassCollected && (pirate.getPosition().x + 15 > compassX) && (pirate.getPosition().x < compassX + compass.getWidth()) && (pirate.getPosition().y + 58 > compassY) && (pirate.getPosition().y < compassY + compass.getHeight())) {
			compassCollected = true;
			audio.render();
		}
		// If the key is not collected yet but the pirate is within its position, then mark as collected and play the audio
		if (!keyCollected && (pirate.getPosition().x + 15> keyX) && (pirate.getPosition().x < keyX + key.getWidth()) && (pirate.getPosition().y + 58 > keyY) && (pirate.getPosition().y < keyY + key.getHeight())) {
			keyCollected = true;
			audio.render();
		}
		// If the chest is not collected yet, but the compass and key are, and the player is within its position, then mark as collected and play the audio
		if (!chestCollected && compassCollected && keyCollected && (pirate.getPosition().x +15 > chestX) && (pirate.getPosition().x < chestX + treasureChest.getWidth()) && (pirate.getPosition().y + 58> chestY) && (pirate.getPosition().y < chestY + treasureChest.getHeight())) {
			chestCollected = true;
			audio.render();
		}
		// If the compass, key and chest are collected, and the player is within its position, then mark as collected and play the audio
		if (compassCollected && keyCollected && chestCollected && (pirate.getPosition().x + 15 > wormholeX) && (pirate.getPosition().x < wormholeX + wormhole.getWidth()) && (pirate.getPosition().y + 58 > wormholeY) && (pirate.getPosition().y < wormholeY + wormhole.getHeight())) {
			dispose();
			gsm.set(new EndState(gsm, player));
		}
	}

	
	/** 
	 * Render the assets relevant to this state
	 * 
	 * @param sb The SpriteBatch to draw the assets
	 */
	@Override
	public void render(SpriteBatch sb) {
		sb.begin();
		// Draw the initial sprites in position
		sb.draw(island, 0, 0, caribbeandiversion.WIDTH, caribbeandiversion.HEIGHT);
		sb.draw(bag, 1100, 10);
		sb.draw(bag, 1150, 10);
		sb.draw(bag, 1200, 10);
		sb.draw(compassBW, 1105, 15);
		sb.draw(keyBW, 1155, 15);
		sb.draw(treasureChestBW, 1205, 15);
		// If the compass is not collected, then draw it
		if (!compassCollected) {
			sb.draw(compass, compassX, compassY);
		}
		// When the compass is collected, redraw in bag
		else {
			sb.draw(compass, 1105, 15);
		}
		// If the key is not collected, then draw it
		if (!keyCollected) {
			sb.draw(key,keyX, keyY);
		}
		// When the key is collected, then draw it in bag
		else {
			sb.draw(key, 1155, 15);
		}
		// If key and compass collected, but chest is not, then draw the chest
		if (compassCollected && keyCollected && !chestCollected) {
			sb.draw(treasureChest, chestX, chestY);
		}
		// If the key, compass and chest are collected, draw the chest to the bag and draw the wormhole
		if (compassCollected && keyCollected && chestCollected) {
			sb.draw(treasureChest, 1205, 15);
			sb.draw(wormhole, wormholeX, wormholeY);
		}
		sb.end();
		// Draw the pirate and update with motion
		pirate.render();
		
	}

	/**
	 * Dispose of all non-essential assets to reduce application memory consumption
	 */
	@Override
	public void dispose() {
		// Dispose of all resources as these are no longer needed and saves application memory
		island.dispose();
		compass.dispose();
		treasureChest.dispose();
		wormhole.dispose();
		audio.dispose();
		pirate.dispose();
	}
}
