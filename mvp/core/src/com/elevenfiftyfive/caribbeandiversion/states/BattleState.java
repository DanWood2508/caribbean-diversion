package com.elevenfiftyfive.caribbeandiversion.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.elevenfiftyfive.caribbeandiversion.BattleAudio;
import com.elevenfiftyfive.caribbeandiversion.ExploreTextLayer;
import com.elevenfiftyfive.caribbeandiversion.TextLayer;

import com.elevenfiftyfive.caribbeandiversion.model.*;

public class BattleState extends State {
    // Manage the masses of assets used in this class
    AssetManager am;
    // This is to manage the Battle Audio
    private BattleAudio audio;
    // Create attributes for all textures
    Texture playerPanel;
    Texture enemyPanel;
    Texture playerShip;
    Texture enemyShip;
    Texture background;
    Texture oceanLayer;
    Texture playerCrewOne;
    Texture playerCrewTwo;
    Texture playerCrewThree;
    Texture enemyCrewOne;
    Texture enemyCrewTwo;
    Texture enemyCrewThree;
    Texture cannonNormal;
    Texture cannonDamaged;
    Texture cannonBroken;
    Texture playerShipDamaged;
    Texture enemyShipDamaged;
    Texture explosion;
    Texture utilityPanel;

    // Groups for all Text in the Battle
    Group playerCrewPanelText;
    Group playerCannonPanelText;
    Group enemyCrewPanelText;
    Group enemyCannonPanelText;
    Group utilityPanelText;
    TextLayer tl;
    Skin skin;
    // The stage on which the items are drawn
    Stage stage;
    // Create the Game States and allow others to be called
    BattleController contr;

    String message;
    Label messsageLabel;

    // The Batch for the explosion animation effect
    SpriteBatch generalGraphicsBatch;
    Texture img;
    // The attributes to allow the Amber Glow to occur
    SpriteBatch amberBatch;
    Texture amberGlow;
    // The attributes to allow the Green Glow to occur
    SpriteBatch greenBatch;
    Texture greenGlow;
    // The attributes which allow the ocean to be animated
    SpriteBatch oceanBatch;
    Texture ocean;
    // The attributes which allow the countdown clock to function
    SpriteBatch clockBatch;
    Texture clock;
    // Attributes allowing the healthbars to move based on health
    SpriteBatch healthBatch;
    Texture health;
    // Animation Frames for the explosion
    TextureRegion[] explosionAnimationFrames;
    Animation<TextureRegion> explosionAnimation;
    // Animation Frames for the Amber Glow
    TextureRegion[] amberAnimationFrames;
    Animation<TextureRegion> amberAnimation;
    // Animation Frames for the Green Glow
    TextureRegion[] greenAnimationFrames;
    Animation<TextureRegion> greenAnimation;
    // Animation frames for the ocean motion
    TextureRegion[] oceanAnimationFrames;
    Animation<TextureRegion> oceanAnimation;
    // Animation for the countdown clock
    TextureRegion[] clockAnimationFrames;
    Animation<TextureRegion> clockAnimation;
    // Animation for the Healthbar
    TextureRegion[] healthAnimationFrames;
    Animation<TextureRegion> healthAnimation;
    // Elapsed time for respective animations
    float elapsedTime;
    float amberElapsedTime;
    float greenElapsedTime;
    float oceanElapsedTime;
    float clockElapsedTime;
    float healthElapsedTime;

    public BattleState(GameStateManager gsm, int difficutly, PlayerShip player) {
        super(gsm);

        contr = new BattleController(new Battle(difficutly, player), audio);
        stage = new Stage();
        generalGraphicsBatch = new SpriteBatch();
        skin = new Skin(Gdx.files.internal("Skins/flat-earth/skin/flat-earth-ui.json"));
        audio = new BattleAudio();

        tl = new TextLayer();
        am = new AssetManager();

        message = "select crew";

        // Add all the necessary textures to the asset manager
        am.load("cancel.png", Texture.class);
        am.load("battle/status-pane.png", Texture.class);
        am.load("battle/friendly.png", Texture.class);
        am.load("battle/enemy.png", Texture.class);
        am.load("battle/background.png", Texture.class);
        am.load("battle/ocean-layer.png", Texture.class);
        am.load("battle/crewOne.png", Texture.class);
        am.load("battle/crewTwo.png", Texture.class);
        am.load("battle/crewThree.png", Texture.class);
        am.load("battle/enemyOne.png", Texture.class);
        am.load("battle/enemyTwo.png", Texture.class);
        am.load("battle/enemyThree.png", Texture.class);
        am.load("battle/cannon-normal.png", Texture.class);
        am.load("battle/cannon-damaged.png", Texture.class);
        am.load("battle/cannon-broken.png", Texture.class);
        am.load("battle/playerShipDamaged.png", Texture.class);
        am.load("battle/enemyShipDamaged.png", Texture.class);
        am.load("battle/explosion.png", Texture.class);
        am.load("battle/utilitypanel.png", Texture.class);
        // load all textures
        am.finishLoading();

        // put all the textures into variables
        playerPanel = am.get("battle/status-pane.png");
        enemyPanel = am.get("battle/status-pane.png");
        playerShip = am.get("battle/friendly.png");
        enemyShip = am.get("battle/enemy.png");
        background = am.get("battle/background.png");
        oceanLayer = am.get("battle/ocean-layer.png");
        playerCrewOne = am.get("battle/crewOne.png");
        playerCrewTwo = am.get("battle/crewTwo.png");
        playerCrewThree = am.get("battle/crewThree.png");
        enemyCrewOne = am.get("battle/enemyOne.png");
        enemyCrewTwo = am.get("battle/enemyTwo.png");
        enemyCrewThree = am.get("battle/enemyThree.png");
        cannonNormal = am.get("battle/cannon-normal.png");
        cannonDamaged = am.get("battle/cannon-damaged.png");
        cannonBroken = am.get("battle/cannon-broken.png");
        playerShipDamaged = am.get("battle/playerShipDamaged.png");
        enemyShipDamaged = am.get("battle/enemyShipDamaged.png");
        explosion = am.get("battle/explosion.png");
        utilityPanel = am.get("battle/utilitypanel.png");

        // Create img and sprite pairs for each animation
        generalGraphicsBatch = new SpriteBatch();
        img = new Texture("explosionCycle.png");
        amberBatch = new SpriteBatch();
        amberGlow = new Texture("amberGlow.png");
        greenBatch = new SpriteBatch();
        greenGlow = new Texture("greenGlow.png");
        oceanBatch = new SpriteBatch();
        ocean = new Texture("oceanSheet.png");
        clockBatch = new SpriteBatch();
        clock = new Texture("clock.png");
        healthBatch = new SpriteBatch();
        health = new Texture("healthSheet.png");

        // create the frame arrays for the differe animations
        TextureRegion[][] tmpFrames = TextureRegion.split(img, 16, 16);
        explosionAnimationFrames = new TextureRegion[8];
        int index = 0;

        TextureRegion[][] redTmpFrames = TextureRegion.split(amberGlow, 45, 45);
        amberAnimationFrames = new TextureRegion[4];
        int redIndex = 0;

        TextureRegion[][] greenTmpFrames = TextureRegion.split(greenGlow, 45, 45);
        greenAnimationFrames = new TextureRegion[4];
        int greenIndex = 0;

        TextureRegion[][] oceanTmpFrames = TextureRegion.split(ocean, 1280, 720);
        oceanAnimationFrames = new TextureRegion[8];
        int oceanIndex = 0;

        TextureRegion[][] clockTmpFrames = TextureRegion.split(clock, 13, 13);
        clockAnimationFrames = new TextureRegion[9];
        int clockIndex = 0;

        TextureRegion[][] healthTmpFrames = TextureRegion.split(health, 79, 12);
        healthAnimationFrames = new TextureRegion[11];
        int healthIndex = 0;

        // for each animation, fill the it's array by adding the necessary frames
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < 8; j++) {
                explosionAnimationFrames[index++] = tmpFrames[i][j];
            }
        }
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < 4; j++) {
                amberAnimationFrames[redIndex++] = redTmpFrames[i][j];
            }
        }
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < 4; j++) {
                greenAnimationFrames[greenIndex++] = greenTmpFrames[i][j];
            }
        }
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 1; j++) {
                oceanAnimationFrames[oceanIndex++] = oceanTmpFrames[i][j];
            }
        }
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < 9; j++) {
                clockAnimationFrames[clockIndex++] = clockTmpFrames[i][j];
            }
        }
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 1; j++) {
                healthAnimationFrames[healthIndex++] = healthTmpFrames[i][j];
            }
        }

        // instantiate the different animations using the frames
        explosionAnimation = new Animation<TextureRegion>(1 / 16f, explosionAnimationFrames);
        amberAnimation = new Animation<TextureRegion>(1 / 16f, amberAnimationFrames);
        greenAnimation = new Animation<TextureRegion>(1 / 16f, greenAnimationFrames);
        oceanAnimation = new Animation<TextureRegion>(1 / 8f, oceanAnimationFrames);
        clockAnimation = new Animation<TextureRegion>(1 / 8f, clockAnimationFrames);
        healthAnimation = new Animation<TextureRegion>(1 / 8f, healthAnimationFrames);

    }

    /**
     * This method determines what keys are pressed, also what positions are clicked by the mouse
     */
    @Override
    protected void handleInput() {
    	/* THIS IS FOR TESTING PURPOSES ONLY
        // if K is pressed, kill the player and kill the battle
        if (Gdx.input.isKeyPressed(Input.Keys.K)) {
            contr.getBattle().getPlayer().suicide();
            contr.killBattle();
        }

        // if W is pressed, kill the enemy and reward the player
        if (Gdx.input.isKeyPressed(Input.Keys.W)) {
            // increase the get player's score
            contr.getBattle().getPlayer().setScore(250);
            // kill the enemy ship
            contr.getBattle().getEnemy().suicide();
            // end the battle
            contr.killBattle();
        }*/

        // clear anything the player has selected
        if (Gdx.input.isKeyJustPressed(Input.Keys.X)) {

            // change the message to cancelled as the player just cleared the selection
            message = "cancelled";
            contr.clearSelection();
            return;

        }

        if (Gdx.input.justTouched()) {
            // Output where the player has selected
            System.out.println(Gdx.input.getX() + " " + Gdx.input.getY());

            // Check if the player selected a player item

            // Compute selected item using getSelected() function
            Utility clicked = getSelected(70, contr.getBattle().getPlayer());

            // If clicked = null it means the player just selected an empty space
            if (clicked != null) {
                // Output what the result of selecting this utility is
                message = contr.select(clicked);
            } else {
                // Otherwise try again to select

                clicked = getSelected(1065, contr.getBattle().getEnemy());
                if (clicked != null) {
                    // If the new selection is not null, then also show the selection
                    message = contr.select(clicked);
                }

            }
            // Check if the player selected an enemy

        }

    }
  
    /** 
     * Continually check for input from the user
     * 
     * @param dt The time period between which the update should be called
     */
    @Override
    public void update(float dt) {
        // Every render check for input
        handleInput();
    }

    /** 
     * Determine the selected utility
     * 
     * @param xStart The x position at which the click was
     * @param ship The ship which this is applicable to
     * @return Utility
     */
    public Utility getSelected(float xStart, Ship ship) {
        // Determine where has been selected based on where the player clicks
        if (Gdx.input.getX() >= xStart && Gdx.input.getX() <= xStart + 40) {

            // Now get the crew member the clicked on
            int Ydifference = 0;

            // Check whether a crew member has been selected by comparing it's position to
            // the selected location
            for (Crew crew : ship.getAllCrew().values()) {
                if (Gdx.input.getY() >= (114 + Ydifference) && Gdx.input.getY() <= (140 + Ydifference)) {

                    System.out.println(crew.getKey());
                    return crew;
                }

                Ydifference += 48;
            }
            Ydifference = 0;
            // Check whether a crew cannon has been selected by comparing it's position to
            // the selected location

            for (Cannon cannon : ship.getAllCannon().values()) {
                if (Gdx.input.getY() >= (271 + Ydifference) && Gdx.input.getY() <= (300 + Ydifference)) {

                    System.out.println(cannon.getKey());
                    return cannon;
                }

                Ydifference += 48;
            }
        }
        return null;
    }

    
    /**
     * This creates the dialogue box to go inside the battle pane
     *  
     * @param crew The crew member to draw the dialogue box for
     * @return Dialog
     */
    // Create they the dialog for a given crew member
    public Dialog createCrewDialog(Crew crew) {
        Dialog dialog = new Dialog(crew.getKey(), skin);

        TextButton test = new TextButton("test", skin);
        dialog.add(test);
        return dialog;
    }

    
    /** 
     * Render all graphics involved, including animation and conditional motion
     * 
     * @param sb The default SpriteBatch to use
     */
    @Override
    public void render(SpriteBatch sb) {
        // Create the different panels and then position them
        playerCrewPanelText = tl.CreateCrewPanel(contr.getBattle().getPlayer().getAllCrew());
        playerCannonPanelText = tl.CreateCannonPanel(contr.getBattle().getPlayer().getAllCannon());
        playerCrewPanelText.setPosition(60, 300);
        playerCannonPanelText.setPosition(50, 50);
        enemyCrewPanelText = tl.CreateCrewPanel(contr.getBattle().getEnemy().getAllCrew());
        enemyCannonPanelText = tl.CreateCannonPanel(contr.getBattle().getEnemy().getAllCannon());
        enemyCrewPanelText.setPosition(1050, 300);
        enemyCannonPanelText.setPosition(1040, 50);

        // Also set the label text using the current message
        messsageLabel = tl.getMessageLabel(message);
        messsageLabel.setPosition(30, 30);

        // Start drawing the explosion.
        generalGraphicsBatch.begin();
        // Draw message
        generalGraphicsBatch.draw(background, 0, 0);
        // Determine whether to draw a damaged or healthy ship
        if (contr.getPlayerShipHealth()) {
            // Draw the playership and position it
            generalGraphicsBatch.draw(playerShip, 200, 140);
        } else {
            // Draw the damaged ship
            generalGraphicsBatch.draw(playerShipDamaged, 200, 140);
        }
        // Draw the ocean
        generalGraphicsBatch.draw(oceanLayer, 0, 0);

        // Draw the player panel
        generalGraphicsBatch.draw(playerPanel, 60, 355);
        // Draw the enemy ship
        generalGraphicsBatch.draw(enemyPanel, 1050, 355);

        // Draw the shooting animations for the cannon. Draw the shooting animation for
        // both the left and right side
        elapsedTime += Gdx.graphics.getDeltaTime();

        for (Cannon cannon : contr.getBattle().getPlayer().getAllCannon().values()) {
            // If the left is currently shooting animate it
            if ((cannon.getCurrent() instanceof Shot) && (Long.valueOf(cannon.getCurrent().getSecsRemaining()) > 0)
                    && (contr.getCannonSide() == "left")) {
                generalGraphicsBatch.draw(explosionAnimation.getKeyFrame(elapsedTime, true), 260, 200);
                generalGraphicsBatch.draw(explosionAnimation.getKeyFrame(elapsedTime, true), 305, 200);
                generalGraphicsBatch.draw(explosionAnimation.getKeyFrame(elapsedTime, true), 359, 200);
            }
            // If the right is currently shooting animate it
            if ((cannon.getCurrent() instanceof Shot) && (Long.valueOf(cannon.getCurrent().getSecsRemaining()) > 0)
                    && (contr.getCannonSide() == "right")) {
                generalGraphicsBatch.draw(explosionAnimation.getKeyFrame(elapsedTime, true), 410, 200);
                generalGraphicsBatch.draw(explosionAnimation.getKeyFrame(elapsedTime, true), 459, 200);
                generalGraphicsBatch.draw(explosionAnimation.getKeyFrame(elapsedTime, true), 510, 200);
            }
        }

        // Draw each cannon and crew symbol
        generalGraphicsBatch.draw(playerCrewOne, 76, 578);
        generalGraphicsBatch.draw(playerCrewTwo, 76, 532);
        generalGraphicsBatch.draw(playerCrewThree, 76, 486);
        generalGraphicsBatch.draw(enemyCrewOne, 1066, 578);
        generalGraphicsBatch.draw(enemyCrewTwo, 1066, 532);
        generalGraphicsBatch.draw(enemyCrewThree, 1066, 486);
        // Determine which graphic to use based upon the cannon key, the function returns the correct texture based upon the cannon health
        generalGraphicsBatch.draw(determinePlayerCannonType("left"), 75, 420);
        generalGraphicsBatch.draw(determinePlayerCannonType("right"), 75, 374);
        generalGraphicsBatch.draw(determineEnemyCannonType("Enemy Right"), 1065, 420);
        generalGraphicsBatch.draw(determineEnemyCannonType("Enemy Left"), 1065, 374);
        // Draw the Text interfaces for the panels
        playerCannonPanelText.draw(generalGraphicsBatch, 1);
        playerCrewPanelText.draw(generalGraphicsBatch, 1);
        enemyCrewPanelText.draw(generalGraphicsBatch, 1);
        enemyCannonPanelText.draw(generalGraphicsBatch, 1);

        // Draw the message
        messsageLabel.draw(generalGraphicsBatch, 1);
        generalGraphicsBatch.end();

        // Create the fade timer
        float fadeTimeAlpha = 1.0f;

        
        clockBatch.begin();
        // For each utility, and draw the timer animation
        for (Utility item : contr.getBattle().getPlayer().getAll()) {
            // Only if the item is not dead
            if (!item.isDead()) {
                if (item.getCurrent() != null) {
                    // If the item is doing something then work out the frame using the time remaining
                    clockBatch.draw(clockAnimationFrames[((Long) item.getCurrent().getSecsRemaining()).intValue()],
                            item.getXCoord() + 50, item.getYCoord() - 2);
                } else {
                	// If the item is doing nothing, then draw an empty clock
                    clockBatch.draw(clockAnimationFrames[0], item.getXCoord() + 50, item.getYCoord() - 2);
                }
            }

        }
        // Cycle through all enemy utilities
        for (Utility item : contr.getBattle().getEnemy().getAll()) {
        	// If the item is not dead
            if (!item.isDead()) {
            	// If the current item is doing something, then draw the frame based on the time remaining
                if (item.getCurrent() != null) {
                    clockBatch.draw(clockAnimationFrames[((Long) item.getCurrent().getSecsRemaining()).intValue()],
                            item.getXCoord() + 50, item.getYCoord() - 2);
                }
                // If the item is doing nothing, then draw an empty clock
                else {
                    clockBatch.draw(clockAnimationFrames[0], item.getXCoord() + 50, item.getYCoord() - 2);
                }
            }
        }
        clockBatch.end();

        // Draw the ocean, this is a constant animation and does not change
        oceanBatch.begin();
        oceanElapsedTime += Gdx.graphics.getDeltaTime();
        oceanBatch.setColor(1.0f, 1.0f, 1.0f, fadeTimeAlpha);
        oceanBatch.draw(oceanAnimation.getKeyFrame(oceanElapsedTime, true), 0, 0);
        oceanBatch.end();

        generalGraphicsBatch.begin();
        // Draw the utility panel text and position this
        generalGraphicsBatch.draw(utilityPanel, 10, 10);
        utilityPanelText = tl.CreateUtilityPanel(contr.getBattle().getPlayer().getResources(),
                contr.getBattle().getPlayer().getMoney(), contr.getBattle().getPlayer().getScore());
        utilityPanelText.setPosition(10, 10);
        utilityPanelText.draw(generalGraphicsBatch, 1);

        // If the enemy ship is of low health, then draw the damaged version, otherwise draw the standard enemy ship
        if (contr.getEnemyShipHealth()) {
            generalGraphicsBatch.draw(enemyShip, 825, 25);
        } else {
            generalGraphicsBatch.draw(enemyShipDamaged, 825, 25);
        }
        generalGraphicsBatch.end();

        healthBatch.begin();
        healthElapsedTime += Gdx.graphics.getDeltaTime();
        healthBatch.setColor(1.0f, 1.0f, 1.0f, fadeTimeAlpha);
        // For all items on the player ship that are not dead, then work out their health, round to the nearest ten and divide by ten to show correct index on sprite sheet
        for (Utility item : contr.getBattle().getPlayer().getAll()) {
            if (!item.isDead()) {
                healthBatch.draw(
                        healthAnimationFrames[10
                                - (int) Math.round((Double.valueOf((((item.getHealth() + 5) / 10) * 10) / 10)))],
                        item.getXCoord() + 50, item.getYCoord() + 17);
            }
        }
        // For all items on the enemy ship that are not dead, then work out their health, round to the nearest ten and divide by ten to show correct index on sprite sheet
        for (Utility item : contr.getBattle().getEnemy().getAll()) {
            if (!item.isDead()) {
                healthBatch.draw(
                        healthAnimationFrames[10
                                - (int) Math.round((Double.valueOf((((item.getHealth() + 5) / 10) * 10) / 10)))],
                        item.getXCoord() + 50, item.getYCoord() + 17);

            }
        }
        
        // Set points to health to 0 for everything and set
        int playerHealth = getPercentage(contr.getBattle().getPlayer());
        healthBatch.draw(healthAnimationFrames[10 - playerHealth], 390, 515);
        int enemyHealth = getPercentage(contr.getBattle().getEnemy());
        healthBatch.draw(healthAnimationFrames[10 - enemyHealth], 1150, 290);
        healthBatch.end();

        amberBatch.begin();
        amberElapsedTime += Gdx.graphics.getDeltaTime();
        amberBatch.setColor(1.0f, 1.0f, 1.0f, fadeTimeAlpha);
        // For every player utility that is not dead, then get the clickable status, if this is clickable then draw the flickering amber graphic to show this to the user
        for (Utility util : contr.getBattle().getPlayer().getAll()) {
            if (!util.isDead()) {
            	// Clickable status determined in BattleController
                if (util.getClickableStatus()) {
                    amberBatch.draw(amberAnimation.getKeyFrame(amberElapsedTime, true), util.getXCoord() - 8,
                            util.getYCoord() - 7);
                }
            }
        }
        // For every enemy utility that is not dead, then get the clickable status, if this is ckickable then draw the flickering amber graphic to show this to the user
        for (Utility util : contr.getBattle().getEnemy().getAll()) {
            if (!util.isDead()) {
            	// Clickable status determined in BattleController
                if (util.getClickableStatus()) {
                    amberBatch.draw(amberAnimation.getKeyFrame(amberElapsedTime, true), util.getXCoord() - 8,
                            util.getYCoord() - 7);
                }
            }
        }
        amberBatch.end();

        greenBatch.begin();
        greenElapsedTime += Gdx.graphics.getDeltaTime();
        greenBatch.setColor(1.0f, 1.0f, 1.0f, fadeTimeAlpha);
        // For every player utility that is not dead, then get the clicked status, if this is clicked then draw the flickering green graphic to show this to the user
        for (Utility util : contr.getBattle().getPlayer().getAll()) {
            if (!util.isDead()) {
            	// Clicked status determined in BattleController
                if (util.getClickedStatus()) {
                    greenBatch.draw(greenAnimation.getKeyFrame(greenElapsedTime, true), util.getXCoord() - 8,
                            util.getYCoord() - 7);
                }
            }
        }
        // For every enemy utility that is not dead, then get the clicked status, if this is clicked then draw the flickering green graphic to show this to the user
        for (Utility util : contr.getBattle().getEnemy().getAll()) {
            if (!util.isDead()) {
            	// Clicked status determined in BattleController
                if (util.getClickedStatus()) {
                    greenBatch.draw(greenAnimation.getKeyFrame(greenElapsedTime, true), util.getXCoord() - 8,
                            util.getYCoord() - 7);
                }
            }
        }
        greenBatch.end();

        // This is drawn below player explosion as LibGDX draws in order, these need to be drawn atop of the enemy ship icon
        generalGraphicsBatch.begin();
        // This will draw the explosion animations if the enemy cannons are fring, and they have more than 0 seconds remaining, the position is determined by the key of the left or right cannon
        for (Cannon cannon : contr.getBattle().getEnemy().getAllCannon().values()) {
            if ((cannon.getCurrent() instanceof Shot) && (Long.valueOf(cannon.getCurrent().getSecsRemaining()) > 0)
                    && (cannon.getKey() == "Enemy Left")) {
                generalGraphicsBatch.draw(explosionAnimation.getKeyFrame(elapsedTime, true), 900, 55);
                generalGraphicsBatch.draw(explosionAnimation.getKeyFrame(elapsedTime, true), 939, 55);
            }
            if ((cannon.getCurrent() instanceof Shot) && (Long.valueOf(cannon.getCurrent().getSecsRemaining()) > 0)
                    && (cannon.getKey() == "Enemy Right")) {
                generalGraphicsBatch.draw(explosionAnimation.getKeyFrame(elapsedTime, true), 1043, 55);
                generalGraphicsBatch.draw(explosionAnimation.getKeyFrame(elapsedTime, true), 1083, 55);
            }
        }
        generalGraphicsBatch.end();

        // If the battle result is not equal to null, then retrieve this
        if (contr.getBattle().getResult() != null) {
            BattleResult result = contr.getBattle().getResult();
            // If the player did not win, then update the top score, kill the battle and dispose resources, then visit the end state where the cause of death can be determined
            if (!result.getWin()) {
                // Save the score in a file
                PlayerShip resultingPlayer = contr.getBattle().getPlayer();
                ExploreTextLayer.updateTopScore(resultingPlayer.getScore());
                contr.killBattle();
                dispose();
                // Pop this from the top of the stack and push the EndState to the top
                gsm.set(new EndState(gsm, resultingPlayer));
    
            } else {
            	// If the player did win, then the result will be applied, the top score will be updated, the battle killed, resources disposed, and the win screen shown
                PlayerShip resultingPlayer = contr.getBattle().getPlayer();
                resultingPlayer.applyResult(result);
                ExploreTextLayer.updateTopScore(resultingPlayer.getScore());
                contr.killBattle();
                dispose();
                // Metrics from battle can be determined in WinState
                gsm.set(new WinState(gsm, resultingPlayer));
            }
        }
    }
    
    /** 
     * Determine which player cannon image to render based upon the health of the cannon
     * 
     * @param orientation The side at which the cannon is sat
     * @return Texture
     */
    private Texture determinePlayerCannonType(String orientation) {
        try {
        	// This checks the health of all player cannons, if this is 80 or more then the healthy graphic will be rendered by calling this method to determine the correct graphic
            if (contr.getBattle().getPlayer().getAllCannon().get(orientation).getHealth() >= 80) {
                return cannonNormal;
            }
            // This checks the health of all cannons, if they are between 80 and 0 a damaged cannon will be shown
            else if (contr.getBattle().getPlayer().getAllCannon().get(orientation).getHealth() > 0) {
                return cannonDamaged;
            }
            // if the health of the cannon is 0 or less, then the broken image will be shown
            else {
                return cannonBroken;
            }
        }
        // If the cannon no longer exists
        catch (Exception e) {
            return cannonBroken;
        }
    }

    /** 
     * Determine which enemy cannon image to draw based upon the health of the cannon
     * 
     * @param orientation The side at which the cannon is sat
     * @return Texture
     */
    private Texture determineEnemyCannonType(String orientation) {
        try {
        	// If the enemy cannon has health of more than or equal to 80, then this shows the normal graphic
            if (contr.getBattle().getEnemy().getAllCannon().get(orientation).getHealth() >= 80) {
                return cannonNormal;
            }
            // If the enemy cannon health is more than 0 but lower than 80 (as above), then the damaged symbol will be shown
            else if (contr.getBattle().getEnemy().getAllCannon().get(orientation).getHealth() > 0) {
                return cannonDamaged;
            }
            // If the health is less than or equal to 0, then the broken symbol will be shown
            else {
                return cannonBroken;
            }
        }
        // Cannon does not exist any more
        catch (Exception e) {
            return cannonBroken;
        }
    }
   
    /** 
     * Determine the percentage health of the ship based upon its utilities
     * 
     * @param ship The ship to calculate the percentage health of
     * @return int
     */
    public int getPercentage(Ship ship) {
    	// Defines two local variables for maximum health and the maximum total health a ship can have
        float totalHealth = 0;
        float totalMaxHealth = 0;
        // Add the health of all utilities, as well as their maximum capable health
        for (Utility util : ship.getAll()) {
            totalHealth += util.getHealth();
            totalMaxHealth += util.getMaxHealth();
        }
        // Calculate a percentage ratio of the two
        float percent = ((totalHealth / (totalMaxHealth)) * 10);

        return (int) percent;
    }

    /**
     * Remove all non-necessary assets to reduce application memory consumption
     */
    @Override
    public void dispose() {
    	// Dispose of all unneeded resources to make application memory usage more efficient
        am.dispose();
        stage.dispose();
        audio.dispose();
    }

}