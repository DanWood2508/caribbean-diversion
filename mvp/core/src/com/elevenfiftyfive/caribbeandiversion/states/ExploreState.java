package com.elevenfiftyfive.caribbeandiversion.states;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.elevenfiftyfive.caribbeandiversion.ExploreAudio;
import com.elevenfiftyfive.caribbeandiversion.ExploreTextLayer;
import com.elevenfiftyfive.caribbeandiversion.caribbeandiversion;
import com.elevenfiftyfive.caribbeandiversion.sprites.ShipSymbol;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.elevenfiftyfive.caribbeandiversion.model.PlayerShip;
// Created just to link button to from MenuState - this should be MapState when implemented!

public class ExploreState extends State {
	// This is the map texture
	private Texture background;
	// This is an array of three island textures
	private Texture[] islands;
	// These are the skulls to indicate difficulty
	private Texture skull;
	// This is the status pane to show metrics
	private Texture metrics;
	// This is the ship itself, which can move
	private ShipSymbol ship;
	// This is a boolean value to determine if the middle (far right) island is selected
	private Boolean farSelected;
	// This is to determine if the boat has moved
	private Boolean moved;
	// This controls to what degree the boat rotates to reach an island naturally
	private float rotation;
	// This is the audio
	private ExploreAudio audio;
	// Text controls for the metrics
	private ExploreTextLayer tl;
	private Group panelText;
	
	// This is for the button to teleport home
	SpriteBatch buttonBatch;
    Texture button;
    // This is the flashing animation of the button to teleport home
    TextureRegion[] animationFrames;
    Animation<TextureRegion> animation;
    float elapsedTime;
    // This is so that the player metrics can be accessed
	PlayerShip player;
	// Default to hard selected island unless told otherwise
	int levelSelection=3;
	
	public ExploreState(GameStateManager gsm, PlayerShip player) {
		super(gsm);
		this.player=player;
		farSelected = false;
		moved = false;
		rotation = 0;
		ship = new ShipSymbol(217, 389);
		audio = new ExploreAudio();
		background = new Texture("new map border.png");
		skull = new Texture("skull.png");
		metrics = new Texture("metrics.png");
		islands = new Texture[3];
		tl = new ExploreTextLayer();
		for (int i = 0; i < islands.length; i++) {
			islands[i] = new Texture("island.png");
		}
		
		buttonBatch = new SpriteBatch();
        button = new Texture("miniGameButton.png");
        // Split the Spritesheet into different frames
        TextureRegion[][] tmpFrames = TextureRegion.split(button, 168, 100);
        // Add this to an array with each slot representing a frame
        animationFrames = new TextureRegion[2];
        int index = 0;
        
        // Move through the frames of the Spritesheet
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < 2; j++) {
                animationFrames[index++] = tmpFrames[i][j];
            }
        }
        animation = new Animation<TextureRegion>(1 / 2f, animationFrames);
	}


	/**
	 * Determine what to do with the input (move the boat to the island)
	 */
	@Override
	public void handleInput() {
		// If the player just clicked on the screen
		if (Gdx.input.justTouched()) {
			if (moved == false) {
				// If the top right hand island was chosen, then the boat will move and stop at x = 780, and will rotate 13 degrees
				if (Gdx.input.getX() > 801 && Gdx.input.getX() < 948 && Gdx.input.getY() < 191 && Gdx.input.getY() > 49) {
					ship.moveTopRight(780);
					rotation = 13;
					moved = true;
					// Queue the cannon sound upon moving
					audio.render();
				}
				// If the middle island was chosen, then the boat will move and stop at x = 945, and will not rotate at all
				else if (Gdx.input.getX() > 999 && Gdx.input.getX() < 1146 && Gdx.input.getY() < 417
						&& Gdx.input.getY() > 272) {
					ship.moveRight(945);
					farSelected = true;
					moved = true;
					rotation = 0;
					// Queue the cannon sound upon moving
					audio.render();
					// Set the level to 2 since this is not the hard island that levelSelection is defaulted to
					levelSelection=2;
				}
				// If the bottom right hand side island was chosen, then the boat will stop at x = 805, and will rotate -17 degrees
				else if (Gdx.input.getX() > 798 && Gdx.input.getX() < 937 && Gdx.input.getY() < 649
						&& Gdx.input.getY() > 497) {
					ship.moveBottomRight(805);
					moved = true;
					rotation = -17;
					// Queue the cannon sound upon moving
					audio.render();
					// Set the level to 1 since this is not the hard island that levelSelection is defaulted to
					levelSelection=1;
				}
				// If the score is more than or equal to 250 and the player clicks in the region of the minigame, then this will be generated
				else if (player.getScore() >= 0 && Gdx.input.getX() > 1076 && Gdx.input.getX() < 1217 && Gdx.input.getY() > 600 && Gdx.input.getY() < 670) {
					dispose();
					gsm.set(new MinigameState(gsm, player));
				}
			}
			// If the ship is at position more than or equal to x = 780 then the battle will be callable
			if (ship.getPosition().x >= 780) {
				gsm.set(new BattleState(gsm,levelSelection,player));
				dispose();
			}
		}
	}

	/** 
	 * Constantly check for input and update the ship
	 * 
	 * @param dt The time period at which the window refreshes
	 */
	@Override
	public void update(float dt) {
		// Constantly check for input and move the ship when needed instantly
		handleInput();
		ship.update(dt);
	}

	
	/** 
	 * Render all associated graphics and motion
	 * 
	 * @param sb The SpriteBatch to be rendered
	 */
	@Override
	public void render(SpriteBatch sb) {
		// Configure the metrics text
		panelText = tl.CreateExplorePanel(player.getResources(),player.getMoney(),player.getScore());
		panelText.setPosition(150, 89);
		
		sb.begin();
		// Clear the default background
		Gdx.gl.glClearColor(0f, 0f, 0f, 0);
		// Draw the background
		sb.draw(background, 0, 0, caribbeandiversion.WIDTH, caribbeandiversion.HEIGHT);
		// Draw the ship
		sb.draw(ship.getTexture(), ship.getPosition().x, ship.getPosition().y, 64, 44, 64, 44, 1, 1, rotation);
		// Draw the metrics pane
		sb.draw(metrics, 80, 50);
		
		// Draw each of the islands in their designated positions
		for (int i = 0; i < islands.length; i++) {
			if (i == 1) {
				sb.draw(islands[i], 1000, (525 - (i * 225)), 150, 150);
			} else {
				sb.draw(islands[i], (800), (525 - (i * 225)), 150, 150);
			}
		}
		
		// Draw each of the skulls in their designated positions
		sb.draw(skull, 855, 150, 22, 22);
		sb.draw(skull, 1050, 375, 22, 22);
		sb.draw(skull, 1075, 375, 22, 22);
		sb.draw(skull, 830, 600, 22, 22);
		sb.draw(skull, 855, 600, 22, 22);
		sb.draw(skull, 880, 600, 22, 22);
		// Draw the metrics text
		panelText.draw(sb, 1);
		sb.end();
		
		// If the player score is more than or equal to 250, then the animated flashing "teleport" button will appear to take the player to the minigame
		if (player.getScore() >= 250) {
			buttonBatch.begin();
	        elapsedTime += Gdx.graphics.getDeltaTime();
	        buttonBatch.setColor(1.0f, 1.0f, 1.0f, 1.0f);
	        buttonBatch.draw(animation.getKeyFrame(elapsedTime, true), 1063, 35);
	        buttonBatch.end();
		}
	}

	/**
	 * Dispose of all non-essential assets to reduce application memory consumption
	 */
	@Override
	public void dispose() {
		// Dispose of all resources not required to be efficient with application memory
		background.dispose();
		metrics.dispose();
		ship.dispose();
		audio.dispose();
		for (int i = 0; i < islands.length; i++) {
			islands[i].dispose();
		}
	}

}
