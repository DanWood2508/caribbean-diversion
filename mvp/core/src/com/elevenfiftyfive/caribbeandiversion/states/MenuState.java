package com.elevenfiftyfive.caribbeandiversion.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.elevenfiftyfive.caribbeandiversion.MenuAudio;
import com.elevenfiftyfive.caribbeandiversion.caribbeandiversion;

public class MenuState extends State{
	// This is the background image
	private Texture background;
	// This is the audio for the menu
	private MenuAudio music;
	
	public MenuState(GameStateManager gsm) {
		super(gsm);	
		background = new Texture("menu.png");
		music = new MenuAudio();
		
	}

	/**
	 * Continually check for input, and if so check to see if a button has been pressed
	 */
	@Override
	public void handleInput() {
		// If the user clicks the mouse
		if (Gdx.input.justTouched()) {
			// If this is on the start button, then begin the storyboard state
			if (Gdx.input.getX() < 1231 && Gdx.input.getX() > 877 && Gdx.input.getY() > 66 && Gdx.input.getY() < 179) {
				// Pass the reference to music to StoryboardState so that the music can continue
				gsm.set(new StoryboardState(gsm, music));
				// Dispose of this state as it is not needed and saves application memory
				dispose(); 
			}
			// If the user presses the quit button to the game
			else if (Gdx.input.getX() < 1231 && Gdx.input.getX() > 877 && Gdx.input.getY() > 191 && Gdx.input.getY() < 305) {
				// Remove this state from the top of the stack
				gsm.pop();
				// Dispose of all items to free up application memory
				dispose();
				music.dispose();
				// Stop the program
				System.exit(0);
			}
		}
	}

	
	/** 
	 * Continually check for input
	 * 
	 * @param dt The time period at which the application updates
	 */
	@Override
	public void update(float dt) {
		handleInput();
	}

	
	/** 
	 * Draw the background for the menu
	 * 
	 * @param sb The SpriteBatch to draw within this method
	 */
	@Override
	public void render(SpriteBatch sb) {
		// Draw the background in a SpriteBatch
		sb.begin();
		sb.draw(background, 0, 0, caribbeandiversion.WIDTH, caribbeandiversion.HEIGHT);
		sb.end();
	}
	
	/**
	 * Dispose of all non-essential resources to reduce application memory usage
	 */
	@Override
	public void dispose() {
		// Dispose of the background image as this is no longer required and will save application memory
		background.dispose();
	}

}
