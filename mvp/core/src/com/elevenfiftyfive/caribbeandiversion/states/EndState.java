package com.elevenfiftyfive.caribbeandiversion.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.elevenfiftyfive.caribbeandiversion.WinLoseAudio;
import com.elevenfiftyfive.caribbeandiversion.caribbeandiversion;
import com.elevenfiftyfive.caribbeandiversion.model.PlayerShip;

public class EndState extends State{
	// This is the image texture
	private Texture image;
	// This is the audio
	private WinLoseAudio audio;
	// This is the player to access health, money, resources and score
	private PlayerShip player;
	BitmapFont font;
	
	public EndState(GameStateManager gsm, PlayerShip player) {
		super(gsm);
		this.player = player;
		
		// If the player dies during battle, show the death by health screen
		if (player.getTotalHealth() <= 0) {
			image = new Texture("deathbyhealth.png");
		}
		// If the player has health, resources and money more than 0 (they should not have died) and thus must have won
		else if (player.getTotalHealth() > 0 && player.getResources() > 0 && player.getMoney() > 0) {
			image = new Texture("winscreen.png");
		}
		// If the player dies due to lack of resources or money, show the death by supplies screen
		else {
			image = new Texture("deathbyresources.png");
		}
		
		// Start the audio on beginning the state
		audio = new WinLoseAudio();
				
		FreeTypeFontParameter	parameter = new FreeTypeFontParameter();
        parameter.size = 50;
        parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
		FreeTypeFontGenerator    generator = new FreeTypeFontGenerator(Gdx.files.internal("Fonts/regfont.ttf"));
		font = generator.generateFont(parameter);
		generator.dispose();
	}

	/**
	 * Determine whether the player has clicked, and if so, close this window
	 */
	@Override
	protected void handleInput() {
		// Tapping anywhere on the screen will close the window
		if (Gdx.input.justTouched()) {
			Gdx.app.exit();	
		}	
	}
	
	/** 
	 * The label details to output the final score of the player before they continue
	 * 
	 * @return Group
	 */
	private Group outputScore() {
		// This will output the score, firstly by configuring the font and its attributes
	
        // The fonts are then placed in a generator so that these can be rendered and applied to a label style
        Label.LabelStyle lbStyle = new Label.LabelStyle();
        lbStyle.font = font;
        lbStyle.fontColor= Color.WHITE;
        // The generator is not needed any more and thus can be disposed of to save application memory
        
        // Make a group that just contains the label which holds the score for the voyage
        Group panel = new Group();
        Label resources = new Label("Your score for this voyage was: "+String.valueOf(player.getScore()), lbStyle);
        // If this is a win, then draw this on the left
        if (player.getTotalHealth() > 0 && player.getResources() > 0 && player.getMoney() > 0) {
        	resources.setPosition(13, 10);
        }
        // If this is a loss, then draw this in the middle
        else {
        	resources.setPosition(350, 10);
        }
        // Add the actor to the group so that it can be rendered
        panel.addActor(resources);
        return panel;
	}

	
	/** 
	 * Constantly check for input
	 * 
	 * @param dt The time period between which the window updates / checks for updates
	 */
	@Override
	public void update(float dt) {
		handleInput();
	}

	/** 
	 * Render the graphics such as the background and score text
	 * 
	 * @param sb The SpriteBatch which is rendered within this method
	 */
	@Override
	public void render(SpriteBatch sb) {
		// Begin the audio
		audio.render();
		// Clear the background and draw the appropriate death / win screen
		sb.begin();
		Gdx.gl.glClearColor(0f, 0f, 0f, 0);
		sb.draw(image, 0, 0, caribbeandiversion.WIDTH, caribbeandiversion.HEIGHT);
		// Draw the label to output the score
		outputScore().draw(sb, 1);
		sb.end();
	}

	/**
	 * Dispose of non-essential assets to reduce application memory consumption
	 */
	@Override
	// Dispose of resources no longer required to release application memory
	public void dispose() {
		audio.dispose();
		image.dispose();
		
	}
			
	
	
}
