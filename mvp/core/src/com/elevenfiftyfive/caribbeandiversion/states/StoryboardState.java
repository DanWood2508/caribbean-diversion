package com.elevenfiftyfive.caribbeandiversion.states;

import java.util.HashMap;	

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.elevenfiftyfive.caribbeandiversion.MenuAudio;
import com.elevenfiftyfive.caribbeandiversion.caribbeandiversion;
import com.elevenfiftyfive.caribbeandiversion.model.Cannon;
import com.elevenfiftyfive.caribbeandiversion.model.Crew;
import com.elevenfiftyfive.caribbeandiversion.model.PlayerShip;

public class StoryboardState extends State {
	// Slide textures
	private Texture currentSlide;
	private Texture slide2;
	private Texture slide3;
	private Texture slide4;
	private Texture slide5;
	// Add the slides to an array to cycle through
	private Texture[] slides = new Texture[] { slide2, slide3, slide4, slide5 };
	// Start with the first slide
	private int index = 0;
	// Store the music for the Storyboards
	private MenuAudio music;

	public StoryboardState(GameStateManager gsm, MenuAudio music) {
		super(gsm);
		// Import the slides to LibGDX
		currentSlide = new Texture("slide1.png");
		slide2 = new Texture("slide2.png");
		slide3 = new Texture("slide3.png");
		slide4 = new Texture("slide4.png");
		slide5 = new Texture("slide5.png");
		slides = new Texture[] { currentSlide, slide2, slide3, slide4, slide5 };
		// Takes the MenuAudio instance from Menu so that audio continues smoothly
		this.music = music;
	}
	
	/** 
	 * Create the player ship with default values ready for the ExploreState so that initial resources can be rendered
	 * 
	 * @return PlayerShip
	 */
	public PlayerShip createShip() {
        // Create the player crew and cannons
        HashMap<String, Crew> crew = new HashMap<>();

        crew.put("edwardo", new Crew(1, "edwardo", 100, 76, 578, true));
        crew.put("pen", new Crew(2, "pen", 100, 76, 532, true));
        crew.put("kidd", new Crew(5, "kidd", 100, 76, 486, true));

        HashMap<String, Cannon> cannons = new HashMap<>();

        cannons.put("left", new Cannon(3, "left", 100, 75, 420, false));
        cannons.put("right", new Cannon(3, "right", 100, 75, 374, false));
        return new PlayerShip(cannons, crew, 30, 50, 10);
    }
	
	/**
	 * Check to see whether the screen has been clicked, if so, move through the slides and at the last one link to the ExploreState and close this state
	 */
	@Override
	protected void handleInput() {
		// If the screen is clocked anywhere, then cycle through the array of slides
		if (Gdx.input.justTouched()) {
			if (index < 4) {
				index++;

			}
			// If this is outside of the bounds of the array, then begin with the ExploreState
			else {
				gsm.set(new ExploreState(gsm,createShip()));
				dispose();
			}
		}
	}

	
	/** 
	 * Continually check for inputs
	 * 
	 * @param dt The rate at which this application refreshes itself
	 */
	@Override
	public void update(float dt) {
		handleInput();
	}

	
	/** 
	 * Draw the assets related to this state
	 * 
	 * @param sb The SpriteBatch required to draw these assets
	 */
	@Override
	public void render(SpriteBatch sb) {
		// This will clear the background and draw the relevant slide based on the index
		sb.begin();
		Gdx.gl.glClearColor(0f, 0f, 0f, 0);
		sb.draw(slides[index], 0, 0, caribbeandiversion.WIDTH, caribbeandiversion.HEIGHT);
		sb.end();

	}

	/**
	 * Dispose of all non-essential assets to reduce application memory consumption
	 */
	@Override
	public void dispose() {
		// Dispose of anything not required to reduce application memory consumption
		music.dispose();
	}

}
