package com.elevenfiftyfive.caribbeandiversion.states;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.elevenfiftyfive.caribbeandiversion.BattleAudio;
import com.elevenfiftyfive.caribbeandiversion.model.*;

public class BattleController {
    // This is the target utility
    private Utility target;
    // This is the using utility in a Task
    private Utility using;
    // String description of the task in progress
    private String task = "";
    // The Battle itself containing the two ships
    private Battle battle;
    private String cannonSide = null;
    // For the battle
    private ExecutorService service;
    
    public BattleController(Battle battle, BattleAudio audio) {
        this.battle = battle;
        // Start the battle
        this.battle = battle;
        service = Executors.newFixedThreadPool(4);
        // The battle is submitted as a service
        service.submit(battle);

    }

    
    /** 
     * Get the target that the utility is currently working on
     * 
     * @return Utility
     */
    public Utility getTarget() {
        // This returns the target utility in the Battle
        return this.target;
    }

    
    /** 
     * Set the target for the utility to work on
     * 
     * @param target The target for the utility to work on
     */
    public void setTarget(Utility target) {
        // This sets the target utility in the Battle
        this.target = target;
    }

    
    /** 
     * Get the utility that the other utility is using
     * 
     * @return Utility
     */
    public Utility getUsing() {
        // This returns the utility being used in the Task within the battle
        return this.using;
    }

    
    /** 
     * Set the utility that the other Utility is using
     * 
     * @param using The utility that the other utility is using
     */
    public void setUsing(Utility using) {
        // This allows you to set the utility to use in the Task within the battle
        this.using = using;
    }

    
    /** 
     * Determine the task a utility is working on
     * 
     * @return String
     */
    public String getTask() {
        // This allows access to the task currently in progress
        return this.task;
    }

    
    /** 
     * Set the task for the utility to complete
     * 
     * @param task The key of the task for the utility to complete
     */
    public void setTask(String task) {
        // This allows the task currently in progress to be set
        this.task = task;
    }

    
    /** 
     * Get the instance of the battle taking place
     * 
     * @return Battle
     */
    public Battle getBattle() {
        // This returns an instance of the battle
        return this.battle;
    }

    
    /** 
     * Set the instance of battle taking place
     * 
     * @param battle The instance of battle to set as the current battle
     */
    public void setBattle(Battle battle) {
        // This allows the battle to be set manually
        this.battle = battle;
    }

    
    /** 
     * Get the service currently being worked on
     * 
     * @return ExecutorService
     */
    public ExecutorService getService() {
        return this.service;
    }

    
    /** 
     * Set the service to be worked on
     * 
     * @param service The service to be worked on
     */
    public void setService(ExecutorService service) {
        this.service = service;
    }

    
    /** 
     * Determine if the target is attackable
     * 
     * @param targetKey The key  of the utility to check
     * @return boolean
     */
    // Determines if a target is attackable by seeing if the Key exists in the
    // enemies utilities and it has health of more than 0
    private boolean attackable(String targetKey) {
        return (battle.getEnemy().getUtil(targetKey) != null && battle.getEnemy().getUtil(targetKey).getHealth() > 0);
    }

    
    /** 
     * Determine if a target is shootable
     *  
     * @param targetKey The key of the utility to check
     * @return boolean
     */
    // Determines if a target is shootable by seeing if it exists in the enemies
    // utilities and the cannon being used is ready to be used
    private boolean shootable(String targetKey) {
        return (battle.getPlayer().getCannon(targetKey) != null && battle.getPlayer().getCannon(targetKey).isReady());
    }

    
    /** 
     * Determine if a target can be set up
     * 
     * @param targetKey The key of the utility to check
     * @return boolean
     */
    // Determines if a target can be set up by ensuring the target is not repairing
    // itself, and the cannon is not in use and is not ready
    private boolean canBeSetup(String targetKey) {
        return (targetKey != using.getKey()) && (battle.getPlayer().getCannon(targetKey) != null)
                && !(battle.getPlayer().getUtil(targetKey)).isReady();
    }

    
    /** 
     * Determine if a target can be repaired
     * 
     * @param targetKey The key of the utility to check
     * @return boolean
     */
    // Determine if something is repairable by ensuring the utility is not repairing
    // itself and the item is damaged
    private boolean repairable(String targetKey) {
        return (targetKey != using.getKey())
                && (isPlayerUtil(targetKey) && battle.getPlayer().getUtil(targetKey).isDamaged());
    }

    
    /** 
     * Determine if a utility is that of the player
     * 
     * @param targetKey The key of the utility to check
     * @return boolean
     */
    // Determines if this is a player utility by checking the key exists in the
    // Player Ship utility HashMap
    private boolean isPlayerUtil(String targetKey) {
        return battle.getPlayer().getUtil(targetKey) != null;
    }

    
    /** 
     * Determine if a crew member is that of the player
     * 
     * @param targetKey The key of the crew member to check
     * @return boolean
     */
    /// Determines if this is a player by checking that the player crew contains
    /// this key
    public boolean isPlayer(String targetKey) {
        if (battle.getPlayer().getAllCrew().containsKey(targetKey)) {
            return true;
        }
        return false;
    }

    
    /** 
     * Determine if something is selectable
     * 
     * @param targetKey The key of the Utility to check
     * @return boolean
     */
    // Determines if something is selectable by firstly ensuring this is a player,
    // and then ensures that this is building a task whereby this is used and this
    // is a shot
    public boolean selectable(String targetKey) {
        isPlayer(targetKey);
        if (isBuildingTask()) {
            if (using.getKey().equals(targetKey)) {
                return false;
            } else if (task.equals("shot")) {
                return attackable(targetKey);
            }
        }
        return true;
    }

    
    /** 
     * Determine if a utility is about to start working on a task
     * 
     * @return boolean
     */
    // Determines if building a task if the using is not equal to null but the task
    // is also not equal to null
    public boolean isBuildingTask() {
        return using != null && task != null;
    }

    /**
     * Clear any selection which has been made
     */
    // Clear selection by setting the target, using and task to null or empty
    public void clearSelection() {
        target = null;
        using = null;
        task = "";
    }

    
    /** 
     * The method to control the selection of a utility from a button click and coordinate front and back end actions from here
     * 
     * @param util The utility which has been selected
     * @return String
     */
    // This method manages selecting a utility
    public String select(Utility util) {

        // Check if the selected utility is dead
        if (battle.getPlayer().getUtil(util.getKey()) != null && battle.getPlayer().getUtil(util.getKey()).isDead()) {
            clearSelection();
            return "Dead";
        }

        // Checks if a crew member has been selected and a shootable cannon is also
        // selected
        if (task.equals("shot")) {
            // This is where the cannons are firing at a crew member
            for (Utility item : getBattle().getEnemy().getAll()) {
                if (item.getKey() != util.getKey()) {
                    // If firing all other enemy utilities are not clickable
                    ((Utility) item).setNotClickable();
                } else {
                    // The utility involved will be set to clicked so that the glowing boxes show
                    // this
                    ((Utility) item).setClicked();
                }
            }

            // Checks to see if the player utility is null
            if (battle.getPlayer().getUtil(util.getKey()) == null) {
                // Apply the shot
                battle.shoot(target.getKey(), util.getKey(), using.getKey());

                // Iterates through all player crew, setting the one selected to clicked and all
                // others to clickable
                for (Utility item : battle.getPlayer().getArrayListCrew()) {
                    if (item.getKey() != util.getKey()) {
                        item.setClickable();
                    }
                }
                String result = "Shot at : " + util.getKey();
                clearSelection();
                return result;
            }
            clearSelection();
            return "suicide";
        }

        if (using == null) {
            // Ensures that a player is selecting their own crew member
            if (util instanceof Crew && getBattle().getPlayer().getUtil(util.getKey()) != null) {
                using = util;
                // Ensures that all player crew who have not been selected and have health of
                // 100 are not clickable
                for (Utility item : getBattle().getPlayer().getAll()) {
                    if (item instanceof Crew && item.getKey() != util.getKey() && item.getHealth() == 100) {
                        ((Utility) item).setNotClickable();
                    }
                    // If the item is equal to the one selected, then set this to clicked
                    else if (item.getKey() == util.getKey()) {
                        ((Utility) item).setClicked();
                    }
                    // If this is an instance of a cannon whose health is less than 100 then this is
                    // also clickable
                    else if (item instanceof Cannon || item.getHealth() < 100) {
                        ((Utility) item).setClickable();
                    }
                }
                return util.getKey() + " selected";
            } else {
                // As a crew member must be first selected to be used
                return "first select crew";
            }
        }
        // Now a target is being selected
        target = util;
        if (this.repairable(util.getKey())) {
            task = "repair";
            this.battle.setRepairTask(using.getKey(), util.getKey());
            if (util instanceof Crew) {
                // Iterate through all Player Crew Members
                for (Utility item : getBattle().getPlayer().getArrayListCrew()) {
                    // If this is not the one selected, then these are not clickable
                    if (item.getKey() != util.getKey()) {
                        ((Utility) item).setNotClickable();
                    }
                    // If this is the one selected, then set this to clicked
                    else {
                        ((Utility) item).setClicked();
                        // If the player contains the item then set the two cannons to not clickable
                        if (getBattle().getPlayer().getAll().contains(item)) {
                            getBattle().getPlayer().getArrayListCannons().get(0).setNotClickable();
                            getBattle().getPlayer().getArrayListCannons().get(1).setNotClickable();
                        }
                    }
                }    
                // Set all enemy crew clickable provided they have not already been clicked
                for (Utility item : battle.getPlayer().getArrayListCrew()) {
                    if (item.getKey() != util.getKey()) {
                        item.setClickable();
                    }
                }
            }
            // If the cannon is selected, set the ones which are not selected to not
            // clickable, the one which is selected then set to clicked
            else if (util instanceof Cannon) {
                for (Utility item : getBattle().getPlayer().getArrayListCannons()) {
                    if (item.getKey() != util.getKey()) {
                        ((Utility) item).setNotClickable();
                    } else {
                        util.setClicked();
                    }
                }
                // Set all player crew to clickable again as these can be clicked once a cannon
                // fire has been locked in
                for (Utility item : getBattle().getPlayer().getArrayListCrew()) {
                    if (item.getKey() != using.getKey()) {
                        ((Utility) item).setClickable();
                    }
                }
            }
            String result = using.getKey() + " repairing " + util.getKey();
            clearSelection();
            return result;
        }
        // If a cannon is selected as a target
        else if (util instanceof Cannon) {
            // The cannon side will be determined, left or right
            this.cannonSide = util.getKey();
            // Go through all player cannons, if they are not the one selected, then these
            // are not clickable, if they are, then this is set to clicked
            for (Utility item : getBattle().getPlayer().getArrayListCannons()) {
                if (item.getKey() != util.getKey()) {
                    ((Utility) item).setNotClickable();
                } else {
                    ((Utility) item).setClicked();
                }
            }
            // If the cannon is shootable, then we will continue to select a target as above
            if (shootable(util.getKey())) {
                target = util;
                task = "shot";
                // Highlight all enemy utilities as clickable
                for (Utility item : getBattle().getEnemy().getAll()) {
                    ((Utility) item).setClickable();
                }
                return "now select target";

            }
            // If the cannon selected after the crew member can be set up, then this will be
            // done
            else if (canBeSetup(util.getKey())) {
                battle.setSetup(using.getKey(), util.getKey());
                // Set all crew members who are not the one selected to be clickable
                for (Utility item : battle.getPlayer().getArrayListCrew()) {
                    if (item.getKey() != util.getKey()) {
                        item.setClickable();
                    }
                }
                String result = using.getKey() + " is setting up " + util.getKey();
                clearSelection();
                return result;
            }
        }
        clearSelection();
        // Iterate through all player utilities
        for (Utility item : getBattle().getPlayer().getAll()) {
            // If this is a cannon, then this is not clickable
            if (item instanceof Cannon) {
                item.setNotClickable();
            }
            // If this is crew, then this is clickable
            else {
                item.setClickable();
            }
            // Unselect the item so that it is clickable again
            item.setNotClicked();
        }

        // Set all enemy utilities to not be clicked
        for (Utility item : getBattle().getEnemy().getAll()) {
            item.setNotClickable();
            item.setNotClicked();
        }
        return "failed select";

    }

    
    /** 
     * Determine the total health of the player ship
     * 
     * @return boolean
     */
    // This will determine the PlayerShip Health by getting the total health, if
    // this is more than 50 this returns true, if not returns false
    public boolean getPlayerShipHealth() {
        if (battle.getPlayer().getTotalHealth() > 50) {
            return true;
        } else {
            return false;
        }
    }

    
    /** 
     * Determine the total health of the enemy ship
     * 
     * @return boolean
     */
    // This will determine the EnemyShip Health by getting the total health, if this
    // is more than 50 this returns true, if not returns false
    public boolean getEnemyShipHealth() {
        if (battle.getEnemy().getTotalHealth() > 50) {
            return true;
        } else {
            return false;
        }
    }

    
    /** 
     * Determine the side at which the cannons are on, left or right
     * 
     * @return String
     */
    // This will return the side of the cannon, left or right
    public String getCannonSide() {
        return cannonSide;
    }

    /**
     * End the battle by force rather than naturally
     */
    // This will stop the battle from occuring and cancel the BattleController as a
    // service
    public void killBattle() {
        service.shutdownNow();

    }

}