package com.elevenfiftyfive.caribbeandiversion.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.elevenfiftyfive.caribbeandiversion.ExploreAudio;
import com.elevenfiftyfive.caribbeandiversion.WinLoseAudio;
import com.elevenfiftyfive.caribbeandiversion.WinTextLayer;
import com.elevenfiftyfive.caribbeandiversion.caribbeandiversion;
import com.elevenfiftyfive.caribbeandiversion.model.PlayerShip;

public class WinState extends State{
	// The background for the win state
	private Texture winimage;
	// The text layer for the win state
	private WinTextLayer tl;
	// Audio for the win state
	private WinLoseAudio audio;
	// The player so that the utility values can be updated
	private PlayerShip resultingPlayer;
	
	public WinState(GameStateManager gsm, PlayerShip resultingPlayer) {
		super(gsm);
		// Import the background
		winimage = new Texture("winimage.png");
		// Begin the audio immediately
		audio = new WinLoseAudio();	
		tl = new WinTextLayer();
		this.resultingPlayer = resultingPlayer;
	}

	/**
	 * Check to see if the screen has been clicked, if so, link back to the ExploreState
	 */
	@Override
	protected void handleInput() {
		// If anywhere on screen is clicked, then return to the ExploreState
		if (Gdx.input.justTouched()) {
			dispose();
        	gsm.set(new ExploreState(gsm, resultingPlayer));
		}
	}

	
	/** 
	 * Continually check for an input
	 * 
	 * @param dt The rate at which this application refreshes
	 */
	@Override
	public void update(float dt) {
		handleInput();
	}

	
	/** 
	 * Draw all assets relevant to the WinState, including Text
	 * 
	 * @param sb The SpriteBatch to draw assets
	 */
	@Override
	public void render(SpriteBatch sb) {
		// Begin rendering the audio
		audio.render();
		// Create the panel so that the winnings can be outputted
		Group panelText = tl.CreateWinPanel(resultingPlayer.getResources(), resultingPlayer.getMoney(), resultingPlayer.getScore());
		// Set the position of the writing
		panelText.setPosition(525, 215);
		sb.begin();
		// Clear the background
		Gdx.gl.glClearColor(0f, 0f, 0f, 0);
		// Draw the background
		sb.draw(winimage, 0, 0, caribbeandiversion.WIDTH, caribbeandiversion.HEIGHT);
		// Draw the text to accompany the background
		panelText.draw(sb, 1);
		sb.end();
		
	}

	/**
	 * Dispose of all non-essential assets to reduce application memory consumption
	 */
	@Override
	public void dispose() {
		// Dispose of anything not needed to save application memory
		audio.dispose();
		winimage.dispose();
	}
}
