package com.elevenfiftyfive.caribbeandiversion;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class WinLoseAudio implements ApplicationListener{
	// Create the attribute which will hold the audio for the Win or Lose screens, both play the same audio
	Music winAudio;
	
	public WinLoseAudio() {
		create();
	}
	
	@Override
	public void create() {
		// Import the audio using the LibGDX import utility
		winAudio = Gdx.audio.newMusic(Gdx.files.internal("winMusic.mp3"));
		// Set this to loop in case the user remains on these screens for an extended period of time, set the volume and get this to play as the class is instantiated
		winAudio.setLooping(true);
		winAudio.setVolume(0.5f);
		winAudio.play();
	}

	
	/** 
	 * Resize the Window
	 * 
	 * @param width Width of the Window
	 * @param height Height of the Window
	 */
	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void render() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		// The audio is now no longer needed, this can be disposed to free up application memory
		winAudio.dispose();
		
	}

}
