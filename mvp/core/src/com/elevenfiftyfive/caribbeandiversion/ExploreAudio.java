package com.elevenfiftyfive.caribbeandiversion;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class ExploreAudio implements ApplicationListener{
	// This loads the Background audio as heard in the explore state
	Music exploreAudio;
	// The Sound is a small snippet of audio which is played when an island is clicked
	Sound exploreSound;
	
	public ExploreAudio() {
		create();
	}
	
	@Override
	public void create() {
		// This imports both soundtracks using the Gdx.files standard
		exploreAudio = Gdx.audio.newMusic(Gdx.files.internal("exploreAudio.mp3"));
		exploreSound = Gdx.audio.newSound(Gdx.files.internal("exploreSound.mp3"));
		
		// Set the audio to loop if the player is on the Explore view for a long period of time
		exploreAudio.setLooping(true);
		// Set the volume of each track, the sound should be significantly louder so that it does not blend in
		exploreAudio.setVolume(0.5f);
		exploreSound.setVolume(0, 3.0f);
		// Begin playing the background audio immediately
		exploreAudio.play();
	}

	
	/** 
	 * Resize the Window
	 * @param width Width of Window
	 * @param height Height of Window
	 */
	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void render() {
		// When the render method is called, the sound will be played once, it should not loop as this is responding to a click event
		exploreSound.play();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		// Both should be disposed as they are no longer required.
		exploreAudio.dispose();
		exploreSound.dispose();
	}

}
