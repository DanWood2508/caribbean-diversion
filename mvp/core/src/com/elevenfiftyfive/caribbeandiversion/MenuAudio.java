package com.elevenfiftyfive.caribbeandiversion;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class MenuAudio implements ApplicationListener {
	// This holds the menu audio itself, this is Music as this is a longer track
	Music menuAudio;
	
	public MenuAudio() {
		create();
	}
	
	@Override
	public void create() {
		// This will import the audio using the LibGDX importing utility
		menuAudio = Gdx.audio.newMusic(Gdx.files.internal("menuAudio.mp3"));
		// Set this to looping in the instance that the user remains on the menu or storyboard for an excessive period of time
		menuAudio.setLooping(true);
		menuAudio.setVolume(0.5f);
		// Play immediately as the class is instantiated
		menuAudio.play();
	}

	
	/** 
	 * @return Boolean
	 */
	public Boolean playing() {
		// If the audio is playing, then return true, this returns the isPlaying() method for external classes which cannot access this
		if(menuAudio.isPlaying() == true) {
			return true;
		}
		return false;
		}

	

	
	/** 
	 * Resize the Window
	 * 
	 * @param width Width of the Window
	 * @param height Height of the Window
	 */
	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void render() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		// Once the audio is called to be disposed, then the only resource used here, the audio itself, should be disposed of to save memory.
		menuAudio.dispose();
		
	}

}
