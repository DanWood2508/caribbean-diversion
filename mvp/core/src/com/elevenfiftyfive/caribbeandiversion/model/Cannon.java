package com.elevenfiftyfive.caribbeandiversion.model;

// Cannon is a utility and thus inherits all of its common methods with Crew
public class Cannon extends Utility {
	// Determine if the cannon is active or not
	private boolean active;
	// The key of what the cannon is shooting at
    private String shooterKey;
    // The shots the cannon has outstanding
    private TaskCollection<Shot> shots;

    public Cannon(int quality, String key,int maxHealth, int xCoord, int yCoord, boolean clickable) {
    	// Configure the cannon as a utlity
        super(quality, key,maxHealth, xCoord, yCoord, clickable);
        this.shooterKey = key;
        active = false;
        shots = new TaskCollection<>();
        //clickable = false;
    }

    /**
     * Attempt to complete the shot if the cannon is not dead, the cannon is not firing but no damage has been applied
     */
    public void attemptCompleting()
    {
    	// If the cannon is not dead, then attempt to complete the current task
    	if (!isDead()) {
    		shots.attemptCurrentCompletetion();
    	}
    }

    
    /** 
     * Checks to see whether the cannon is set up or not
     * 
     * @return boolean
     */
    public boolean isSetUp() {
    	// Check if the cannon has been setup and thus is ready for battle
        return this.isReady();
    }

    
    /** 
     * Manually set the cannon's setUp status
     * 
     * @param setUp The desired cannon setUp status
     */
    public void setSetUp(boolean setUp) {
    	// Check to see if the cannon is not dead, and if it is not then set this to ready
        if(!this.isDead())
        {
            this.setReady(setUp);
        }
    }

    
    /** 
     * Checks to see if a cannon has shot, if the Shot TaskCollection includes the utility key
     *
     * @param targetKey The utility to check if has been shot
     * @return boolean
     */
    public boolean hasShot(String targetKey)
    {   
    	// See if the shot has been fired or if it is still outstanding in the Shot class
        return this.shots.has(targetKey);
    }
    
    
    /** 
     * Checks to see if a cannon is idle or not
     * 
     * @return boolean
     */
    public boolean isIdle() {
    	// If the cannon is dead, then it cannot be idle, if it is currently doing nothing, then it is idle
        if (isDead()) {
            return false;
        }
        return (getCurrent() == null);
    }

    
    /** 
     * Gets the key of the crew member shooting the cannon
     * 
     * @return String
     */
    public String getShooterKey() {
    	// This is a getter method for the shooterKey
        return shooterKey;
    }

    
    /** 
     * Get the current task the cannon is doing
     * 
     * @return Task
     * @throws NullPointerException In the case that the Cannon does not exist any more but has not been marked as dead
     */
    public Task getCurrent() throws NullPointerException {
    	// If the cannon is dead then it is doing nothing, if not, then return the current shot
        if (isDead()) {
            return null;
        }
        return shots.getCurrent();

    }

    
    /** 
     * Manually set the shooter key, the crew member shooting the cannon
     * 
     * @param shooterKey The key of the crew member shooting the cannon
     */
    public void setShooterKey(String shooterKey) {
    	// To set the cannon to shoot at a utility then the shooterKey needs to be set
        this.shooterKey = shooterKey;
    }

    /**
     * Cancells a shot by removing the shot next in the TaskCollection queue
     */
    public void cancelShot() {
    	// Set the shot to null so that this is no longer processed
    	this.shots.start(null);
    }
    
    
    /** 
     * Applies a shot by deducting health and making the cannon not ready
     * 
     * @param targetUtility The key of the utility being targetted
     * @return Utility
     */
    public Utility applyShot(Utility targetUtility) {
    	// If the cannon dies in the meantime then this will return null and thus will be cancelled
        if (isDead()) {
            return null;
        }
        // If not, apply the shot
        setReady(false);
        return shots.applyTask(this, targetUtility);
    }
    
    
    /** 
     * Gets the cannon to shoot provided it is ready and not dead, sets the cannon to active and starts shooting
     * 
     * @param shooterKey The key of the crew member shooting the cannon
     * @param targetKey The key of the utility being targeted by the cannon
     */
    public void shoot(String shooterKey, String targetKey) {
    	// To shoot, this ensures that the cannon is ready and is not dead
        if (isReady() && !isDead()) {
        	// Make the cannon active so that the glow changes and it cannot be clicked
        	toggleActive();
        	// Start the shot, providing the quality of shot and the targetKey
            shots.start(new Shot(30 - (this.getQuality() - 10), targetKey));
            this.setShooterKey(shooterKey);
        }
    }
    
    /**
     * Invert the current active status of the cannon
     */
    public void toggleActive() {
    	// Invert the current active status
    	active = !(active);
    }
    
    
    /** 
     * Determine if the cannon is in use or not
     * 
     * @return boolean Desired status of the cannon
     */
    public boolean getActive() {
    	// Check to see if the cannon has shot or not
    	return hasShot(this.getKey());
    	
    }
}