package com.elevenfiftyfive.caribbeandiversion.model;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class Ship {
    private HashMap<String, Crew> crew;
    private HashMap<String, Cannon> cannons;
    private int totalHealth;

    public Ship(HashMap<String, Cannon> cannons, HashMap<String, Crew> crew) {
    	// This gives the Ship Cannons, Crew, Name and TotalHealth
        this.cannons = cannons;
        this.crew = crew;
        totalHealth = this.getTotalHealth();
     
    }

    
    /** 
     * Stage the death of a utility by adjusting all values to make this class as dead
     * 
     * @param dead The utility to set to being dead
     */
    public void deathOfAUtil(Utility dead) {
    	// The dead utility is passed in, and the death will be recorded, so this is toggled
        dead.toggleDeathRecorded();
        // If the dead utility is a Cannon then cancel all shots by the Crew Member using it
        if (dead instanceof Cannon) {
            // Cancel all shots
            for (Crew crewMem : crew.values()) {
                if (crewMem.getCurrent() != null && crewMem.getCurrent().getTargetKey() == dead.getKey()) {
                    crewMem.cancelCurrent();
                }
            }
        } 
        // If the dead utility is a Crew Member, then cancel the shots scheduled by the Cannon it is using
        else if (dead instanceof Crew) {
            for (Cannon cannon : cannons.values())
                if (cannon.getCurrent() != null && cannon.getShooterKey() == dead.getKey()) {
                    cannon.cancelShot();
                }
        }
    }

    
    /** 
     * Replace a utility with another identical utility, this is if the utility is being repaired
     * 
     * @param util The utility to replace
     */
    public void replaceUtil(Utility util) {
    	// If the utility passed is an instance of Cannon, then place this in the Cannon HashMap with its Key
        if (util instanceof Cannon) {
            cannons.put(util.getKey(), (Cannon) util);
        } 
        // If the utility passed is an instance of Crew, then place it in the CrewHashMap with its Key
        else {
            crew.put(util.getKey(), (Crew) util);
        }
    }
    
    /** 
     * Start a shooting task
     * 
     * @param usingCannon The key of the cannon shooting
     * @param shooter The key of the crew member shooting the cannon
     * @param target The key of the target utility of the cannon
     */
    public void setShootingTask(String usingCannon, String shooter, String target) {
    	// To start a shooting attack, get the cannon being used, and then apply a shot
        this.cannons.get(usingCannon).shoot(shooter, target);
    }

    /** 
     * Determine if all cannons on the Ship have been destroyed
     * 
     * @return boolean
     */
    public boolean allCannonsDestroyed() {
    	// Count how many cannons are destroyed, or have health of 0 or less
        int count=0;
        for (Cannon cannon : this.getAllCannon().values()) {
            if (cannon.isDead()) {
                count++;
            }
        }
        // Return whether this is equivalent to the number of cannons in total
        return this.getAllCannon().size() == count;
    }
    
    
    /** 
     * Set the total health of the ship
     * 
     * @param health The total health to set to this value
     * @return int
     */
    public int setTotalHealth(int health) {
    	// Update the total health of the ship
    	this.totalHealth = health;
    	return totalHealth;
    }
    
    /** 
     * Checks to see if a utility has been set up or not
     * 
     * @param targetKey The key of the utility to check
     * @return boolean
     */
    public boolean isSetUp(String targetKey) {
    	// If the cannons HashMap contains the cannon in question
    	if (cannons.containsKey(targetKey)) {
    		// Check if the cannon in question is set up
	    	if (cannons.get(targetKey).isSetUp()) {
	    		return false;
	    	}
	    	return true;
    	}
    	return true;
    }

    
    /** 
     * Checks to see if all crew members on the Ship are dead
     * 
     * @return boolean
     */
    public boolean allCrewDead() {
    	// Counts how many crew members are dead or have health of 0 or less in the Crew HashMap
        int count=0;
        for (Crew crewM : this.getAllCrew().values()) {
            if (crewM.isDead()) {
                count++;
            }
        }
        // See if the size of the crew HashMap is equal to the number dead
        return this.getAllCrew().size() == count;
    }

    
    /** 
     * Set a repair task of a utility on the Ship
     * 
     * @param target The target of the repair task
     * @param crew The crew member coordinating the repair task
     */
    public void setRepairTask(Utility target, Crew crew) {
        // Check if they are operating a gun first. If they are end the shooting of that cannon
        for (Cannon cannon : cannons.values()) {
            if (cannon.getShooterKey() == crew.getKey()) {
                cannon.setShooterKey("");
            }
        }
        // If the cannon target is not being operated, but is damaged, and the crew member is not the one being shot at then begin the repair
        if (!isBeingOperated(target.getKey()) && target.isDamaged() &&crew.getKey()!=target.getKey()) { 
            crew.startRepair(target.getKey());
        }
    }

    
    /** 
     * Get a HashMap of all crew members, done by key of crew
     * 
     * @return HashMap<String, Crew>
     */
    public HashMap<String, Crew> getAllCrew() {
    	// Return crew HashMap
        return crew;
    }

    
    /** 
     * Get a HashMap of all cannons, done by key of cannons
     * 
     * @return HashMap<String, Cannon>
     */
    public HashMap<String, Cannon> getAllCannon() {
    	// Return cannon HashMap
        return cannons;
    }

    
    /** 
     * Get the cannon from a key
     * 
     * @param targetKey The key of the cannon to be returned
     * @return Cannon
     */
    public Cannon getCannon(String targetKey) {
    	// Get a cannon from the cannon HashMap by Key
        return cannons.get(targetKey);
    }
    
    
    /** 
     * Get a crew member from a key
     * 
     * @param targetKey The key of the crew member to be returned
     * @return Crew
     */
    public Crew getCrew(String targetKey) {
    	// Get a crew member from the crew HashMap by key
        return crew.get(targetKey);
    }

    
    /** 
     * Checks to see if a utility is being used
     * 
     * @param utilityKey The key of the utility to check
     * @return boolean
     */
    public boolean isBeingOperated(String utilityKey) {
        // Check if a crew member is preparing this item
        for (Crew crewM : crew.values()) {
            if (crewM.getCurrent() != null && crewM.getCurrent().getTargetKey().equals(utilityKey)) {
                return true;
            }
        }
        return false;
    }
    
    /** 
     * Begin a setup task
     * 
     * @param targetKey The key of the target utility
     * @param crew The crew member coordinating the setup task
     */
    public void setSetupTask(String targetKey, Crew crew) {
        // End whatever targetKey is doing first
        for (Cannon cannon : cannons.values()) {
            if (cannon.getShooterKey() == crew.getKey()) {
                cannon.setShooterKey("");
                cannon.cancelShot();
            }

        }
        // If the utility is not being operated and this is a cannon, then the crew member can begin setting the cannon up
        if (!isBeingOperated(targetKey) && getUtil(targetKey) instanceof Cannon) {
            crew.startSetupCannon(targetKey);
        }
    }

    
    /** 
     * Get a utility using a key
     * 
     * @param key The key of the utility to find
     * @return Utility
     * @throws NullPointerException If the utility no longer exists
     */
    public Utility getUtil(String key) throws NullPointerException
    // Searches all utilities and returns on a key match, has to search both HashMaps as Cannon and Crew are Utilities
    {
        if (cannons.containsKey(key)) {
            return cannons.get(key);
        } else if (crew.containsKey(key)) {
            return crew.get(key);
        }
        return null;
    }

    /** 
     * Checks to see if a ship has a Utility present
     * 
     * @param key The key to search for
     * @return boolean
     */
    public boolean hasUtil(String key) {
    	// If the cannon is being used by something, then return it, if not, try with crew
        if (cannons.containsKey(key)) {
            return true;
        } else {
            return crew.containsKey(key);
        }
    }
    
    /**
     * Sets all utilities on a ship to have health 0, to essentially kill the ship
     */
    public void suicide() {

        for (Utility util : this.getAll()) {
            util.setHealth(0);
        }
	}
    
    /**
     * Update all utilities with what they should be doing, and attempt completion of tasks
     */
    public void update() {
        // Complete and apply tasks
        for (String cannonKey : cannons.keySet()) {
            for (String crewKey : crew.keySet()) {
                // Cheese int from, int 310000
                // Check if this crew member has repaired this cannon
                if (crew.get(crewKey).hasRepaired(cannonKey)) {
                    Cannon applied = (Cannon) crew.get(crewKey).applyRepair(cannons.get(cannonKey));
                    cannons.put(cannonKey, applied);
                }
              
                // Check if this crew member has setup this cannon
                if (crew.get(crewKey).hasSetup(cannonKey)) {
                    Cannon applied = (Cannon) crew.get(crewKey).applySetup(cannons.get(cannonKey));
                    cannons.put(cannonKey, applied);
                }

            }
            cannons.get(cannonKey).attemptCompleting();
         
        }

        // Check if the crew member have repaired any other crew member
        for (String crewKey : crew.keySet()) {
            for (String otherMemberKey : crew.keySet()) {
            	// Checks to see if the crew member has repaired the crew member in question
                if (crew.get(otherMemberKey).hasRepaired(crewKey)) {
                	// Apply the repair between the crew members, which updates their values
                    Crew applied = (Crew) crew.get(otherMemberKey).applyRepair(crew.get(crewKey));
                    // Put the recovered crew member into the Crew HashMap
                    crew.put(crewKey, applied);
                }
            }
        }

        // Complete current tasks
        for (String key : crew.keySet()) {
            crew.get(key).finishCurrent();
        }
    }

   
   /** 
    * Get an ArrayList of Cannons
    * 
    * @return ArrayList<Cannon>
    */
    // This returns an ArrayList of Cannons by iterating through the HashMap and adding to an ArrayList
    public ArrayList<Cannon> getArrayListCannons() {
        ArrayList<Cannon> arrCannons = new ArrayList<>();
        for (Cannon cannon : cannons.values()) {
            arrCannons.add(cannon);
        }

        return arrCannons;
    }

    
    /** 
     * Get an ArrayList of Crew Members
     * 
     * @return ArrayList<Crew>
     */
    // This returns an ArrayList of Crew Members by iterating through the HashMap and adding to an ArrayList
    public ArrayList<Crew> getArrayListCrew() {
        ArrayList<Crew> arrCrew = new ArrayList<>();
        for (Crew crewMem : crew.values()) {
            arrCrew.add(crewMem);
        }

        return arrCrew;
    }
    
    
    /** 
     * Get an ArrayList of all Utilities on the Ship
     * 
     * @return ArrayList<Utility>
     */
    // This iterates through both the Crew and Cannon HashMaps and adds these to a collective ArrayList
    public ArrayList<Utility> getAll() {
    	ArrayList<Utility> all = new ArrayList<>();
    	for (Crew crewMem : crew.values()) {
    		all.add(crewMem);
    	}
    	for (Cannon cannon : cannons.values()) {
    		all.add(cannon);
    	}
    	return all;
    }
    
    
    /** 
     * Get the total health of the Ship (derived from all utility health)
     * @return int
     */
    public int getTotalHealth() {
    	totalHealth = 0;
    	// Iterate through the ArrayList of Crew
    	for (Crew member: getArrayListCrew()) {
    		// If the member is not dead then add to the health the percentage of the crew members health
            if (!member.isDead()) {
                totalHealth += 100*(member.getHealth()/member.getMaxHealth());
            }
    	}
    	// Iterate through the ArrayList of Cannons
    	for (Cannon can: getArrayListCannons()) {
    		// If the cannon is not dead then add to the health the percentage of crew members health
            if (!can.isDead()) {
    		totalHealth += 100*(can.getHealth()/can.getMaxHealth());}
    	}
    	// Divide this by five to calculate an average health
    	return totalHealth/5;
    }
}