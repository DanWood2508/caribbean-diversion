package com.elevenfiftyfive.caribbeandiversion.model;

import java.util.*;

public class TaskCollection<T extends Task> {
	// HashMap to hold all completed tasks
    private HashMap<String, T> completeTasks;
    // A Type Variable to hold the current task
    private T current;

    public TaskCollection() {
    	// Instantiate the completeTasks HashMap
        completeTasks = new HashMap<>();
    }

    
    /** 
     * Check to see if a task contains a certain target
     * 
     * @param targetKey The key of the target being searched for
     * @return boolean
     */
    public boolean has(String targetKey) {
    	// See if the complete tasks contains the targetKey
        return completeTasks.keySet().contains(targetKey);
    }

    
    /** 
     * Begin the task at hand
     * 
     * @param task The task which should be started
     */
    public void start(T task) {
    	// Start the task by setting the current task to the parameter task
        this.current = task;
    }

    
    /** 
     * Get the current worker on a task
     * 
     * @return String
     */
    public String currentWorker() {
    	// Get the key of the crew member on the task
        return current.getTargetKey();
    }

    
    /** 
     * Get the current Task being completed
     * 
     * @return Task
     */
    public Task getCurrent() {
    	// Get the current task being completed
        return this.current;
    }

    /**
     * Attempt to complete the curren task if this is marked as done but still reains as the current task and is in the active TaskCollection
     */
    public void attemptCurrentCompletetion() {
    	// Attempt to complete when the current task is done but is not marked as null
        if (current != null && current.isDone()) {
        	// Put the task in completeTasks
            completeTasks.put(current.getTargetKey(), current);
            // Set current to null ready for the next task
            current = null;
        }
    }

    
    /** 
     * Apply a task by calling the specialised method but also remove this from the TaskCollection and change clickable ratings of Utilities involved
     * 
     * @param usingUtil The utility being used for a task
     * @param target The utility which is the subject of the task
     * @return Utility
     */
    public Utility applyTask(Utility usingUtil, Utility target) {
    	// Set the result to the application of the task within completeTasks with the target key
        Utility result = completeTasks.get(target.getKey()).apply(usingUtil, target);
        
        // Remove the task after application
        completeTasks.remove(target.getKey());
        // Set the two utilities involved to not be clicked
        usingUtil.setNotClicked();
        target.setNotClicked();

        return result;
    }

    
    /** 
     * Determine the completed tasks, return in a HashMap
     * 
     */
    public HashMap<String, T> getCompleteTasks() {
    	// Get the complete tasks by returning the HashMap of complete tasks
        return this.completeTasks;
    }
}