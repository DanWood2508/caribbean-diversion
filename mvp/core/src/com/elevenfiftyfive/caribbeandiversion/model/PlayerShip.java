package com.elevenfiftyfive.caribbeandiversion.model;

import java.util.HashMap;

public class PlayerShip extends Ship {
	// Configure the attributes of the Ship, which represents the player
    private int resources;
    private int money;
    private int health;
    private int score;
	

    public PlayerShip(HashMap<String, Cannon> cannons, HashMap<String, Crew> crew, int resources,
            int money, int health) {
    	// Send the common attributes to the Ship superclass, which is shared with EnemyShip, common elements of a Ship
        super(cannons, crew);
        this.resources = resources;
        this.money = money;
        this.health = health;
    }

    
    /** 
     * Determine what resources are available for the player
     * 
     * @return int
     */
    public int getResources() {
    	// This returns the number of resources the player has
        return this.resources;
    }
    
    /**
     * Decrement the amount of resources the player has
     */
    public void reduceResources(){
    	// This decrements the number of resources the player has
        resources--;
    }
    
    /** 
     * Determine what money the player has
     * 
     * @return int
     */
    public int getMoney() {
    	// This returns the amount of money the player has
        return this.money;
    }
    
    /**
     * Decrement the amount of money the player has
     */
    public void reduceMoney() {
		// This will decremend the amount of money the player has
        this.money--;
	}
    
    
    /** 
     * Set the score to a desired value
     * 
     * @param score The desired value of the score
     */
    public void setScore(int score) {
    	// This sets the score of the player to the value passed as a parameter
        this.score += score;
    }
    
    /** 
     * Determine the score of the player
     * 
     * @return int
     */
    public int getScore() {
    	// This returns the score of the player
        return this.score;
    }

    
    /** 
     * Determine the health of the player
     * 
     * @return int
     */
    public int getHealth() {
    	// This returns the health of the PlayerShip
        return this.health;
    }

    
    /** 
     * Apply the result of a battle to each of the individual player attributes
     * 
     * @param result The result of the battle to be applied to the player
     */
    public void applyResult(BattleResult result) {
    	// To apply the result, the result will be taken, and the appropriate value will be incremented by the reward returned by BattleResult
        
        if (result.getDifficulty()==1) {
            this.resources += 20;
            this.money += 50;
            this.score +=result.getScore();
        }else 
        if (result.getDifficulty()==2) {
            this.resources += 30;
            this.money += 60;
            this.score +=result.getScore();
        }else{
            this.resources += 60;
            this.money += 100;
            this.score +=result.getScore();
        }
    }

    
    /** 
     * Checks to see if the player has run out of utility attributes
     * 
     * @return boolean
     */
    public boolean resourceDry() {
    	// If the player is resource dry, then they have no or negative health, money and resources
        return this.health <= 0 || this.money <= 0 || this.resources <= 0;
    }
}