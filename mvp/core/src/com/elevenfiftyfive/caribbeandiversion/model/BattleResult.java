package com.elevenfiftyfive.caribbeandiversion.model;

import java.util.Random;

public class BattleResult {
	// Create the attributes to add to the battle
    private boolean win;
    private int points;
    private int moneyReward;
    private int resourceReward;
    private int score;
    private int difficulty;

    public BattleResult(boolean win, int points, int moneyReward, int resourceReward, int score, int difficulty) {
    	// The result will be passed whether this is a win or not, the points, money, resource and score 
        this.win = win;
        this.points = points;
        this.moneyReward = moneyReward;
        this.resourceReward = resourceReward;
        this.score = score;
        this.difficulty=difficulty;
    }
    
    /** 
     * Returns the difficulty of the battle
     * 
     * @return int
     */
    public int getDifficulty(){
        return difficulty;
    }

    /**
     * A constructor for the BattleResult which only uses difficulty, this is for when the game has finished and no other parameters can be provided
     * @param difficulty The integer difficulty of the battle chosen
     */
    // If the battle result is required before the game has finished, there will be no parameters provided and thus there will be no result
    public BattleResult(int difficulty) {
        this.difficulty=difficulty;
    }

    
    /** 
     * This generates what reward the player gets based upon the quality of the tools used
     * 
     * @param quality The integer quality representation of the utility in use
     * @return int
     */
    public int generateRewardCount(int quality) {
    	// Based on the quality, the greater the reward, ie a level 3 quality generates a larger reward than a level 1
      
        switch (quality) {
            case 1:
                return 5;
            case 2:
                return 10;
            case 3:
                return 15;
                
            
        }
        return 0;
    }

    
    /** 
     * Generates a random reward based on quality
     * 
     * @param quality The integer value of the quality of the utility being used, determines reward given.
     */
    public void reward(int quality) {
    	// Generate a random quality rating 1-3 to determine how good the hit was
        Random rnd = new Random();
        int pick = rnd.nextInt(3);
        // Depending on the type of action, this will generate either a random money or resource reward
       
        this.resourceReward = resourceReward + generateRewardCount(quality);

        this.moneyReward = moneyReward + 50;
             
        // Add to the score based upon the quality provided
        switch (quality) {
            case 1:
                this.score += 20;
                break;
            case 2:
                this.score += 100;
                break;
            case 3:
                this.score += 200;
                break;
        }
    }

    
    /** 
     * Returns the total score for the game thus far
     * 
     * @return int
     */
    public int getScore() {
    	// Return the score calculated thus far
        return this.score;
    }

    
    /** 
     * Sets the score for the game so far, mainly used for back-door and testing purposes
     * 
     * @param score The desired score of the game
     */
    public void setScore(int score) {
    	// Set the score
        this.score = score;
    }

    
    /** 
     * Checks to see if a battle has been won
     * 
     * @return boolean
     */
    public boolean isWin() {
    	// Check to see if this is a win or not
        return this.win;
    }

    
    /** 
     * Checks to see if a battle has been won
     * 
     * @return boolean
     */
    public boolean getWin() {
        return this.win;
    }

    
    /**
     * Manually sets a battle to won so that a final result can be calculated
     * 
     * @param win Whether the battle was a success or not (win / lose)
     */
    public void setWin(boolean win) {
    	// Set this to a win if the battle is won, thus the final result can eb calculated
        this.win = win;
    }

    
    /** 
     * Get the number of points for a battle
     * 
     * @return int
     */
    public int getPoints() {
    	// Get the number of points
        return this.points;
    }

    
    /** 
     * Set the number of points for a battle
     * 
     * @param points The desired point count
     */
    public void setPoints(int points) {
    	// Set the number of points
        this.points = points;
    }

    
    /** 
     * Retrieve the monitary reward for an action
     * 
     * @return int
     */
    public int getMoneyReward() {
    	// Get the monitary reward for an action
        return this.moneyReward;
    }

    
    /** 
     * Set the monitary reward for an action
     * 
     * @param moneyReward The integer reward for completing an action
     */
    public void setMoneyReward(int moneyReward) {
    	// Set the monitary reward for an action
        this.moneyReward = moneyReward;
    }

    
    /** 
     * Get the resource reward for an action
     * 
     * @return int
     */
    public int getResourceReward() {
    	// Get the resource reward for an action
        return this.resourceReward;
    }

    
    /** 
     * Set the resource reward for an action
     * 
     * @param resourceReward The desired resource reward
     */
    public void setResourceReward(int resourceReward) {
    	// Set the resource reward for an action
        this.resourceReward = resourceReward;
    }

    
    /** 
     * Sets and returns the win passed through
     * 
     * @param win Boolean to represent whether battle was win or loss
     * @return BattleResult
     */
    public BattleResult win(boolean win) {
    	// Update the win status and then the result will be calculated and returned
        this.win = win;
        return this;
    }

    
    /** 
     * Set and return the points for a battle
     * 
     * @param points The desired number of points for a battle
     * @return BattleResult
     */
    public BattleResult points(int points) {
    	// Update the points and then calculate a result and return this
        this.points = points;
        return this;
    }

    
    /** 
     * Set and return the points for a battle
     * 
     * @param moneyReward The desired monetary reward for a battle
     * @return BattleResult
     */
    public BattleResult moneyReward(int moneyReward) {
    	// Update the monitary reward and calculate a result and return this
        this.moneyReward = moneyReward;
        return this;
    }

    
    /** 
     * Set and return the resource reward for a battle
     * 
     * @param resourceReward The desired resource reward for a battle
     * @return BattleResult
     */
    public BattleResult resourceReward(int resourceReward) {
    	// Update the resource reward and calculate a result and return this
        this.resourceReward = resourceReward;
        return this;
    }

    
    /** 
     * See if one reward is identical to another reward
     * 
     * @param o - the other reward being compared to
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof BattleResult)) {
            return false;
        }
        // Check to see if two Results are equal, check this based upon the win status, points, money and resource rewards
        BattleResult battleResult = (BattleResult) o;
        return win == battleResult.win && points == battleResult.points && moneyReward == battleResult.moneyReward
                && resourceReward == battleResult.resourceReward;
    }

    
    /** 
     * Convert the result to a string for outputting
     * 
     * @return String
     */
    @Override
    public String toString() {
    	// Convert the current result to string if required
        return "{" + " win='" + isWin() + "'" + ", Points='" + getPoints() + "'" + ", moneyReward='" + getMoneyReward()
                + "'" + ", resourceReward='" + getResourceReward() + "'" + "}";
    }

}