package com.elevenfiftyfive.caribbeandiversion.model;
public class Repair extends Task{
   
    public Repair(int length, String targetKey) {
        super(length,targetKey);
    }

    
    /** 
     * Applies the repair task
     * 
     * @param usingUtil The utility being used to repair
     * @param targetUtil The utility being repaired
     * @return Utility
     */
    @Override
    // Changes the TargetUtil using the UsingUtil
    public Utility apply(Utility usingUtil,Utility targetUtil) { 
        // Add some health back
        targetUtil.resetHealth();  // min = current health + 5   
        // If the target utility is an instance of crew, then set this to be clickable so that it glows again
        if(targetUtil instanceof Crew) {
        	targetUtil.setClickable();
        }
        return targetUtil;
        
    }
}