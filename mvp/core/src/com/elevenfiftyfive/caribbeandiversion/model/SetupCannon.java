package com.elevenfiftyfive.caribbeandiversion.model;
public class SetupCannon extends Task{
   
    public SetupCannon(int length, String targetKey) {
    	// This passes the common values to Task, which is shared with Repair, Shot, etc.
        super(length,targetKey);
    }

    
    /** 
     * Sets the cannon up
     * @param usingUtil The utility being used to set the cannon up
     * @param targetUtil The cannon which is being setting up
     * @return Utility
     */
    @Override
    // Changes the TargetUtil using the UsingUtil
    public Utility apply(Utility usingUtil,Utility targetUtil) { 
        targetUtil.setReady(true);  
        return targetUtil; 
    }  
}