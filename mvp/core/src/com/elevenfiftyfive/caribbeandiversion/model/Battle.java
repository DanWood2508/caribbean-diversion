package com.elevenfiftyfive.caribbeandiversion.model;

import java.util.HashMap;
import java.util.Random;

public class Battle implements Runnable {
    // Create the attributes which will be used throughout
    private EnemyShip enemy;
    private PlayerShip player;
    private boolean testing = false;
    private BattleResult result;
    private boolean isOver;
    private int difficulty;

    public Battle(int difficutly, PlayerShip player) {
        // Call the constructor of the superclass
        super();
        // The result will be stored in teh BattleResult class, so this needs
        // instantiating
        result = new BattleResult(difficutly);
        // This is to determine if the battle is over (one of the sides win)
        isOver = false;
        // The difficulty rating based upon the island selected
        this.difficulty = difficutly;
        // Details about the player such as resources, money and score
        this.player = player;

        enemy = createEnemy(difficutly);

    }

    public void toggleTesting() {
        testing = !testing;
    }

    /**
     * Applies damage to the any utilities which are due to be damaged
     */
    public void applyDamages() {
        // Check if the enemy has shot a player util
        for (Cannon eCannon : enemy.getAllCannon().values()) {

            // Check if the enemy has shot any of the player cannons
            for (Cannon pCannon : player.getAllCannon().values()) {
                if (eCannon.hasShot(pCannon.getKey())) {
                    // Apply the shot so that the cannon gets shot
                    player.replaceUtil(eCannon.applyShot(pCannon));

                }
                // If the cannon is not marked as "dead" but it has health of zero or less, then
                // this should be killed to avoid NullPointerExceptions
                if (!pCannon.isAlreadyDead()) {
                    if (pCannon.getHealth() <= 0) {
                        player.deathOfAUtil(pCannon);
                    }
                }
            }

            // This will iterate through all crew members
            for (Crew pCrew : player.getAllCrew().values()) {
                // If an enemy cannon has shot the crew member (based on key)
                if (eCannon.hasShot(pCrew.getKey())) {
                    // Apply the shot to the crew member so that their health is damaged
                    player.replaceUtil(eCannon.applyShot(pCrew));
                }
                // If the crew member is not marked as dead but their health is of zero or less,
                // then this should be killed
                if (!pCrew.isAlreadyDead()) {
                    if (pCrew.getHealth() <= 0) {
                        player.deathOfAUtil(pCrew);
                    }
                }
            }
        }

        // Check if the player has shot the enemy
        for (Cannon pCannon : player.getAllCannon().values()) {

            // Check if the enemy has shot any of the player cannons
            for (Cannon eCannon : enemy.getAllCannon().values()) {
                // Check if the player Cannon has shot an enemy cannon by checking key (name)
                if (pCannon.hasShot(eCannon.getKey())) {
                    // If they have, then apply the shot to the enemy cannon so that their health
                    // decreases
                    enemy.replaceUtil(pCannon.applyShot(eCannon));
                    // Get the crew member involved in the shooting and set as not clicked so that
                    // they begin to glow clickable again
                    player.getCrew(pCannon.getShooterKey()).setNotClicked();
                    // Set the player and enemy cannon involved as not clicked so that they will
                    // show as clickable again when applicable
                    pCannon.setNotClicked();
                    eCannon.setNotClicked();
                }
                // If the enemy cannon already has health of zero or more, then it will be
                // killed to avoid exceptions and errors later
                if (!eCannon.isAlreadyDead()) {
                    if (eCannon.getHealth() <= 0) {
                        enemy.deathOfAUtil(eCannon);
                        // Apply the reward as the player has killed an enemy utility
                        this.result.reward(difficulty);
                    }
                }
            }

            // Iterate through all members of the enemy crew
            for (Crew eCrew : enemy.getAllCrew().values()) {
                // If a player cannon has shot an enemy crew member based on key (name)
                if (pCannon.hasShot(eCrew.getKey())) {
                    // Apply the shot to the enemy crew member
                    enemy.replaceUtil(pCannon.applyShot(eCrew));
                    // Once this is done, set the shooter to be not clicked so that they will glow
                    // as clickable again
                    player.getCrew(pCannon.getShooterKey()).setNotClicked();
                    // Set the two cannons involved as clickable so that they can be selected
                    // whenever appropriate
                    pCannon.setNotClicked();
                    eCrew.setNotClicked();
                }
                // If the enemy crew member is not already dead but has health of less than or
                // equal to 0, then they should be killed to avoid exceptions
                if (!eCrew.isAlreadyDead()) {
                    if (eCrew.getHealth() <= 0) {
                        enemy.deathOfAUtil(eCrew);
                        // As the player has killed an enemy crew member, they get their reward
                        this.result.reward(difficulty);
                    }
                }
            }
        }
    }

    
    /** 
     * Shoots a cannon by reducing resources and money, but also starting a shooting task
     * 
     * @param usingCannon The key of the cannon currently in use
     * @param target The key of the utility being targeted
     * @param shooter The key of the crew member usinfg the cannon
     */
    public void shoot(String usingCannon, String target, String shooter) {
        // If the enemy being shot at is not dead, then the resources of the player
        // should be reduced and the shooting should begin
        try {
            if (!enemy.getUtil(target).isDead()) {
                this.player.reduceResources();
                this.player.reduceMoney();
                this.player.setShootingTask(usingCannon, shooter, target);
            }
        } // This means that one of the utilities involved in combat has died, this will
          // have been picked up above too at the end of the task
        catch (NullPointerException e) {
            // one of the utils died
        }
    }

    
    /** 
     * Sets a repair task for the target, provided either involved are not dead. Also deducts funds.
     * 
     * @param crew The key of the crew member currently doing the repair
     * @param target The key of the item which is being repaired
     */
    public void setRepairTask(String crew, String target) {
        // The utility to repair and the crew member being used will be added to local
        // variables for ease
        Utility repair = player.getUtil(target);
        Crew using = player.getCrew(crew);
        // Provided neither of the utilities are dead (have 0 or less health) then they
        // will be repaired and their funds will be reduced
        if (!using.isDead() && !repair.isDead()) {
            this.player.reduceMoney();
            player.setRepairTask(repair, using);
        }
    }

    
    /** 
     * Checks that the repairer are not damaged, begins the repair and reduces money.
     * 
     * @param crew The key of the crew member doing the repair
     * @param target The key of the utility being repaired
     */
    public void setSetup(String crew, String target) {
        // Set the crew member attempting to set up the utility to a local variable for
        // ease
        Crew using = player.getCrew(crew);
        // Check that the crew is not dead, if they are not then they will setup the
        // utility they are using and have their funds deducted
        if (!using.isDead()) {
            this.player.reduceMoney();
            player.setSetupTask(target, using);
        }
    }

    
    /** 
     * Returns an instance of the player ship
     * 
     * @return PlayerShip
     */
    public PlayerShip getPlayer() {
        // Return an instance of the player ship
        return player;
    }

    
    /** 
     * Returns an instance of the enemy ship
     * 
     * @return EnemyShip
     */
    public EnemyShip getEnemy() {
        // Return an instance of the enemy ship
        return enemy;
    }

    
    /** 
     * Converts the battle to string by converting the player to string
     * 
     * @return String
     */
    public String toString() {
        // Convert the player to string
        return player.toString();
    }

    /**
     * Ensures that every item is updated, damages are applied, and checks for wins and losses
     */
    public void update() {
        // This is for testing purposes!
        // System.out.print("running: " + System.currentTimeMillis() + "\n");
        // Keep updating everything so that this is done in real time
        player.update();
        enemy.update();
        enemy.makeMove(player);
        // Apply all damages waiting to be applied, this will be done almost instantly
        // from the users' perspective
        applyDamages();

        // Check if the player lost
        if (player.allCannonsDestroyed() || player.allCrewDead() || player.resourceDry()) {
            // Set the game to be over and set this as a loss as one element of the player
            // ship has died, or they have run out of resources
            this.isOver = true;
            this.result.setWin(false);
        }
        // Check if the player won
        if (enemy.allCannonsDestroyed() || enemy.allCrewDead()) {
            // Set this to a win and begin ending the game, as this only runs if the game is
            // not over
            this.result.setWin(true);
            this.isOver = true;
        }
    }

    
    /** 
     * Generates a new health for an item based on the difficulty of the island chosen
     * 
     * @param difficulty The difficulty of the selected island
     * @return int
     */
    public int generateHealth(int difficulty) {
        Random rnd = new Random();
        // Depending on the difficulty chosen, the harder the battle, the higher the
        // enemy utility's health is, to make the battle harder, this is a random amount
        // to reduce predictability
        switch (difficulty) {
            case 1:
                return rnd.nextInt(30) + 5;

            case 2:
                return rnd.nextInt(50) + 30;
            case 3:
                return rnd.nextInt(20) + 80;

        }
        return 0;
    }

    
    /** 
     * Creates the enemy which is being played against
     * 
     * @param difficulty The integer difficulty of the island chosen, determines speed of enemy for added difficulty
     * @return EnemyShip
     */
    private EnemyShip createEnemy(int difficulty) {
        // Add all of the crew to a HashMap so that they can be accessed by key
        HashMap<String, Crew> crew = new HashMap<>();
        // Add all of the cannons to a HashMap so that they can be accessed by key
        HashMap<String, Cannon> cannons = new HashMap<>();

        // Add the crew and cannons to the relevant HashMap, generate their random
        // health and drawing coordinates on the panels
        crew.put("dave", new Crew(1, "dave", generateHealth(difficulty), 1066, 578, false));
        crew.put("john", new Crew(2, "john", generateHealth(difficulty), 1066, 486, false));
        crew.put("james", new Crew(5, "james", generateHealth(difficulty), 1066, 532, false));

        
        if (difficulty == 1) {
            cannons.put("Enemy Left", new Cannon(10, "Enemy Left", generateHealth(difficulty), 1065, 374, false));
            cannons.put("Enemy Right", new Cannon(10, "Enemy Right", generateHealth(difficulty), 1065, 420, false));
    
            return new EnemyShip(7, cannons, crew);
        } else if (difficulty == 2) {
            cannons.put("Enemy Left", new Cannon(30, "Enemy Left", generateHealth(difficulty), 1065, 374, false));
            cannons.put("Enemy Right", new Cannon(30, "Enemy Right", generateHealth(difficulty), 1065, 420, false));
    
            return new EnemyShip(4, cannons, crew);
        } else {
            cannons.put("Enemy Left", new Cannon(30, "Enemy Left", generateHealth(difficulty), 1065, 374, false));
            cannons.put("Enemy Right", new Cannon(30, "Enemy Right", generateHealth(difficulty), 1065, 420, false));
    
            return new EnemyShip(2, cannons, crew);

        }

    }

    
    /** 
     * Returns the result of the battle if this is over
     * 
     * @return BattleResult
     */
    public BattleResult getResult() {
        // If the battle is over, then return the result, if not, return null as there
        // is no total score yet
        if (isOver) {
            return result;
        } else {
            return null;
        }
    }

    @Override
    public void run() {
        // Keep updating all resources
        update();
        // Add a wait between updates otherwise the game would be impossible to play
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {

            e.printStackTrace();
        }
        // Keep running recursively until the game is over
        if (!isOver) {
            run();
        }
    }
}