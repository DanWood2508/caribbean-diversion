package com.elevenfiftyfive.caribbeandiversion.model;

import java.time.LocalDateTime;
import java.time.Duration;

public abstract class Task {
    private int length;
    private LocalDateTime end;
    private String targetKey;

    public Task(int length, String targetKey) {
    	// The length of the task
        this.length=5;
        // Set time at which the task ends
        this.end = LocalDateTime.now().plusSeconds(this.length);
        // Set the target key of the Task
        this.setTargetKey(targetKey);
    }

    
    /** 
     * Get the length (duration) of a given task
     * 
     * @return int
     */
    public int getLength() {
    	// Get the length of the task
        return length;
    }

    
    /** 
     * Set the length (duration) of a given task
     * 
     * @param length The desired length of a task
     */
    public void setLength(int length) {
    	// Set the length of the task
        this.length = length;
    }

    
    /** 
     * Get the target key of a task
     * 
     * @return String
     */
    public String getTargetKey() {
    	// Get the target key
        return targetKey;
    }

    
    /** 
     * Set the target key of a task
     * 
     * @param targetKey The desired target of the task
     */
    public void setTargetKey(String targetKey) {
    	// Set the target key
        this.targetKey = targetKey;
    }

    
    /** 
     * Determine how long is remaining of a task
     * 
     * @return long
     */
    public long getSecsRemaining() {
    	// Get the length of time remaining of a task, the time between the time now and the end time
        long secsRemaining = Duration.between(LocalDateTime.now(), end).getSeconds();
        if (secsRemaining <= 0) {
            return 0;
        } else {
            return secsRemaining;
        }
    }
    
    
    /** 
     * Determines whether a task has been completed or not
     * 
     * @return boolean
     */
    // Checks to see if the task is complete or not, complete if the time remaining is 0
    public boolean isDone() {
        return getSecsRemaining() == 0;
    }
    
    // Every Task can be applied, but differently
    abstract public Utility apply(Utility usingUtil, Utility targetUtil);
}