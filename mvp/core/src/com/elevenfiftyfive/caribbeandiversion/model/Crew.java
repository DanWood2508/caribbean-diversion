package com.elevenfiftyfive.caribbeandiversion.model;
import java.util.HashMap;

// A member of crew is a utility like a Cannon, both cannon and crew will share some methods and implementations
public class Crew extends Utility {
	// These are the TaskCollections of Repairs and Setups the Crew Member must complete
    private TaskCollection<Repair> repairs;
    private TaskCollection<SetupCannon> setups;

    public Crew(int quality,String Key, int maxHealth, int xCoord, int yCoord, boolean clickable) {
    	// Pass the common values to the superclass to process and deal with
        super(quality,Key,maxHealth, xCoord, yCoord, clickable);

        repairs = new TaskCollection<Repair>();
        setups = new TaskCollection<SetupCannon>();
    }

    
    /** 
     * Checks to see if the crew member has repaired the target
     * 
     * @param targetKey The utility being checked to see if it has been repaired
     * @return boolean
     */
    public boolean hasRepaired(String targetKey) {
    	// See if the target key is in the TaskCollection of outstanding repair jobs
        return repairs.has(targetKey);
    }
    
    /**
     * Removes the current next repair and setup being undertaken
     */
    public void cancelCurrent()
    {
    	// This calls will cancel the current repair or setup in progress
        this.cancelRepair();
        this.cancelSetup();
    }
    
    /** 
     * Checks to see if the crew member has set up a target key
     * 
     * @param targetKey The target key being checked to see if set up
     * @return boolean
     */
    public boolean hasSetup(String targetKey) {
    	// This checks if the outstanding TaskCollections of setups contains the key we are querying
        return setups.has(targetKey);
    }

    
    /** 
     * Apply the setup task by calling this of the setups class
     * 
     * @param targetUtility The key of the utility being set up
     * @return Utility
     */
    public Utility applySetup(Utility targetUtility) {
    	// To apply the setup, we apply the task on the targetKeys in the setups TaskCollection
        return setups.applyTask(this, targetUtility);
    }

    
    /** 
     * Apply the repair of a utility
     * 
     * @param targetUtility The key of the utility being set up
     * @return Utility
     */
    public Utility applyRepair(Utility targetUtility) {
    	// To apply the repair, we apply the task on the targetKeys in the setups TaskCollection
        return repairs.applyTask(this, targetUtility);
    }

    
    /** 
     * Begin the repair job of the target
     * 
     * @param targetKey The key of the utility being repaired (crew or cannon)
     */
    public void startRepair(String targetKey) {
        // Start the repair process on all repairs waiting in the TaskCollection, also consider the quality of the repair
        repairs.start(new Repair(20 - (this.getQuality() / 10), targetKey));
    }

    /**
     * Finisht the current task in progress by attempting completion of awaiting repairs and setups, happens if complete but reward not applied
     */
    // Attempt the ending of the current tasks by attempting to complete all outstanding repairs and setups
    public void finishCurrent() {
        repairs.attemptCurrentCompletetion();
        setups.attemptCurrentCompletetion();
    }

    
    /** 
     * Begins the setup of a cannon using the setups class
     * 
     * @param targetKey The key of the cannon being setup
     */
    public void startSetupCannon(String targetKey) {
    	// This begins setting up a cannon using the SetupCannon class
        setups.start(new SetupCannon(20 - (this.getQuality() / 10), targetKey));
    }

    
    /** 
     * Get a HashMap of comopleted repairs by key of target and ID of repair
     * 
     * @return HashMap<String, Repair> HashMap of Repair IDs with the Target Utility Key being the Key
     */
    public HashMap<String, Repair> getCompleteRepairs() {
    	// This gets the complete tasks by using the TaskCollection
        return repairs.getCompleteTasks();
    }
    
    
    /** 
     * Get a HashMaps of completed setups by key of target and ID of setup utility
     * 
     * @return HashMap<String, SetupCannon> HashMap of Setup IDs with Target Utility Key being the Key
     */
    public HashMap<String, SetupCannon> getCompleteSetups() {
    	// This will query the TaskCollection to see what complete tasks are available
        return setups.getCompleteTasks();
    }

    
    /** 
     * Checks to see if the crew member is idle by checking if they have a current task
     * 
     * @return boolean
     */
    public boolean isIdle() {
    	// Checks to see if the crew member has a current, if not then it must be idle
        return (getCurrent() == null);
    }

    
    /** 
     * Gets the current task of the crew member
     * 
     * @return Task
     */
    public Task getCurrent() {
    	// If there is a current setup and this is not done, then this is the current task
        if (setups.getCurrent() != null && !setups.getCurrent().isDone()) {
            return setups.getCurrent();
        }
        // If there is a current repair job in the repairs TaskCollection, and this is not finished, then this is the current task
        else if(repairs.getCurrent() != null && !repairs.getCurrent().isDone())
        {
            return repairs.getCurrent();
        }
        // If the crew member is not doing a repair or setup, then it must be doing nothing, return null
        return null;   
    }

    /**
     * Cancels the repair job at the front of the repair queue
     */
    public void cancelRepair() {
    	// Set the value at the start of the repairs TaskCollection to null
        this.repairs.start(null);
    }
    
    /**
     * Cancels the setup at the front of the setup queue
     */
    public void cancelSetup() {
    	// Set the value at the start of the repairs TaskCollection to null
        this.setups.start(null);
    }

}