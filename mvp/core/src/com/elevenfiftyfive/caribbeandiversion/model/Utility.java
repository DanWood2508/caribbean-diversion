package com.elevenfiftyfive.caribbeandiversion.model;

public abstract class Utility {
    private String key;

    private int health;
    private int quality;
    private int maxHealth;
    private boolean ready;
    private boolean active;
    
    private int xCoord;
    private int yCoord;
    protected boolean clickable;
    protected boolean clicked = false;

    private boolean deathRecorded;
    public Utility(int quality, String key,int maxHealth, int xCoord, int yCoord, boolean clickable) {
    	// The key of the utility to uniquely identify this
        this.key = key;
        // The quality of the utility
        this.quality = quality;
        // See if the utility is ready for the next task
        this.ready = false;
        // The maximum health of the utility
        this.maxHealth=maxHealth;
        // Set the initial health to maximum health as we assume this is brand new
        this.health = maxHealth;
        // The utility is not active by default
        active = false;
        // The X and Y coordinates will be passed to this class
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        // The utility clickable attribute is passed to this class
        this.clickable = clickable;
        // See if the death has been recorded of this utility
        deathRecorded=false;
    }
    
	
    /** 
     * @return boolean
     */
    public boolean isAlreadyDead() {
		// See if a death has been recorded
		return deathRecorded;
	}
    public void setClickable() {
    	// Set clickable to true
    	clickable = true;
    }
    
    public void setNotClickable() {
    	// Set clickable to false
    	clickable = false;
    }
    
    
    /** 
     * @return boolean
     */
    public boolean getClickableStatus() {
    	// If the utility is dead it is not clickable, otherwise return clickable
        if (isDead()) {
            return false;
        }
    	return clickable;
    }
    
    public void setClicked() {
    	// if the item is clicked then set this to true so the glow changes and clickable to false so that this no longer renders as clickable
    	clicked = true;
    	clickable = false;
    }
    
    public void setNotClicked() {
    	// Set clicked to false which will in turn swap clickable to true when next appliable
    	clicked = false;
    }
    
    
    /** 
     * @return boolean
     */
    public boolean getClickedStatus() {
    	// Return whether or not the utility has been clicked
    	return clicked;
    }
    
    /** 
     * @return boolean
     */
    public boolean isReady() {
    	// Check if the utility is ready for use or not
        return ready;
    }
    
    
    /** 
     * @return int
     */
    public int getXCoord() {
    	// Get the x coordinate of the utility
    	return xCoord;
    }
    
    
    /** 
     * @return int
     */
    public int getYCoord() { 
    	// Get the y coordinate of the utility
    	return yCoord;
    }
    
    /** 
     * Set whether the Utility is ready
     * 
     * @param ready The desired ready state of the Utility
     */
    public void setReady(boolean ready) {
    	// Set the utility to be ready if this is about to be used
        this.ready = ready;
    }

    
    /** 
     * @return String
     */
    public String getKey() {
    	// Return the unique key of the utility
        return key;
    }

    
    
    /** 
     * Set the key of the utility
     * 
     * @param key The desired key of the utility
     */
    public void setKey(String key) {
    	// Change the unique key of the utility
        this.key = key;
    }
    
    /**
     * Determine whether the utility is damaged or not
     *  
     * @return boolean
     */
    public boolean isDamaged() {   
    	// If the utility this cannot be damaged any further, if not, this checks to see if the health is less than 80%
        if (isDead()) {
            return false;
        }
        return ((health/maxHealth)*100) < 80;
    }
    
    /** 
     * Determine the maximum health of the utility
     * 
     * @return int
     */
    public int getMaxHealth() {
    	// This returns the maximum health for the utility
        return this.maxHealth;
    }
    
    /** 
     * Determine the current health of the utility
     * 
     * @return int
     */
    public int getHealth() {
    	// Return the health of the utility
        return health;
    }

    
    /** 
     * Determine the quality of the utility
     * 
     * @return int
     */
    public int getQuality() {
    	// Return the quality of the utility
        return quality;
    }

    
    /** 
     * Set the health of the utility
     * 
     * @param health The desired health of the utility
     */
    public void setHealth(int health) {  
    	// If the utility is not dead, then update the health, cannot change health of a dead utility
        if(!isDead())
        {
            this.health = health;
        }
    }
    
    /**
     * Reset the health back to the maximum health of the utility
     */
    public void resetHealth() {
    	// If the utility is not dead, then set the health to maximum, a utility cannot be reset once dead
        if(!isDead())
        {
            this.health = maxHealth;
        }  
    }
    
    /**
     * Set the utility active or not, invert the current state
     */
    public void toggleActive() {
    	// Invert the active status of the utility
    	active = !(active);
    }
    
    
    /** 
     * Determine whether the utility is currently active
     * 
     * @return boolean
     */
    public boolean getActive() {
    	// Determine if the utility is active or not
    	return active;
    }
    
    
    /** 
     * Determine the current utility the utility is working on
     * 
     * @param isDead(
     * @return Task
     */
    // Every utility is able to have a current task, but the way of determining this varies with type of utility
    public abstract Task getCurrent();

	
    /** 
     * Determine whether the utility is dead or not
     * 
     * @return boolean
     */
    public boolean isDead() {
		// Checks to see if the health is less than or equal to zero
		return this.health<=0;
	}

    /**
     * Set whether the utility is dead or not, inverts current state
     */
	public void toggleDeathRecorded() {
		// Invert the death recorded
        this.deathRecorded=true;
	}
}