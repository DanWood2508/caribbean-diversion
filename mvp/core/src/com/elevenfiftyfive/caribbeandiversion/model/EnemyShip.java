package com.elevenfiftyfive.caribbeandiversion.model;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class EnemyShip extends Ship {
	// The Time of the last move that the enemy ship made, to control movement frequency
    private LocalDateTime lastMove;
    private Random rnd;
    // Speed at which the enemy works at, dependent on the level of island chosen
    int speed;
    
    public EnemyShip(int speed,HashMap<String, Cannon> cannons, HashMap<String, Crew> crew) {
    	// Pass the common values to the superclass, which is shared with PlayerShip
        super(cannons, crew);
        // Set the last move to the time at which this is instantiated
        lastMove = LocalDateTime.now();
        rnd = new Random();
        // Sets the speed, the time between tasks, based upon the difficulty of island selected
        this.speed=speed;
    }

    
    /** 
     * Begins to repair the utility by selecting an idle member of crew and a utility which requires repairing
     * 
     * @return String
     */
    public String repairUtility() {
    	// Makes a local variable for the utility which needs repairing and the crew member repairing
        Utility needsRepair = needsRepair();
        Crew repairer = selectIdleCrew();
        if (needsRepair != null && repairer != null) {
            // Random to decide whether this will repair or not
            if (rnd.nextBoolean()) {
                this.setRepairTask(needsRepair, repairer);
                return needsRepair.getKey() + " is repairing " + repairer.getKey();
            }
        }
        // If there is no need for repair or idle crew
        return "";
    }

    
    /** 
     * Selects an idle member of crew, a ready cannon and a random player target and begins to shoot at them
     * 
     * @param opponent The opponent of the enemy (player)
     * @return String
     */
    public String shootEnemy(Ship opponent) {
    	// Create a local variable for the shooter and the cannon which is ready
        Crew shooter = selectIdleCrew();
        Cannon readyCannon = selectCannon(true);
        // Provided that there are idle crews and free cannons, then shoot
        if (shooter != null && readyCannon != null) {
            // Select a random component to target
            String target;
            // Choose between Crew and Cannon, random chance
            if (rnd.nextBoolean()) {
            	// Add all of the Player Cannons to an ArrayList and set a random one of these as the target
                ArrayList<Cannon> allOppCannons = opponent.getArrayListCannons();
                target = allOppCannons.get(rnd.nextInt(allOppCannons.size())).getKey();
            } else {
            	// Add all of the Player Crew Members to an ArrayList and set a random one of these as the target
                ArrayList<Crew> allOppCrew = getArrayListCrew();
                target = opponent.getArrayListCrew().get(rnd.nextInt(allOppCrew.size())).getKey();
            }
            // If the opponent target is not dead, then the shooting can begin
            if (!opponent.getUtil(target).isDead()) {
                this.setShootingTask(readyCannon.getKey(), shooter.getKey(), target);
                return shooter.getKey() + " shooting " + readyCannon.getKey() + " at " + target;
            }
        }
        // If there were no free crew members or cannons
        return "";
    }

    
    /** 
     * Configures a cannon by seeking an idle member of crew and a cannon requiring setting up
     * @return String
     */
    public String setupCannon() {
    	// Create a local variable to store a random idle member of crew and cannon which needs setting up
        Crew crew = selectIdleCrew();
        Cannon needsSetupCannon = needsSetup();
        // If there is idle crew and a cannon requiring setup, then begin setting this up
        if (crew != null && needsSetupCannon != null) {
            this.setSetupTask(needsSetupCannon.getKey(), crew);
            return crew.getKey() + " is setting up " + needsSetupCannon.getKey();
        }
        // If there are no idle crew membrs or cannons requiring setup
        return "";
    }

    
    /** 
     * Makes a random move provided that there has been sufficient spacing
     * 
     * @param opponent The opponent Ship object
     * @return String
     */
    public String makeMove(Ship opponent) {
    	// Checks to see if it can move again, if the time since the last move exceeds the break between based upon the island difficulty
        if (secsSinceLastMove() >= speed) {
        	// If a move can be made, then set this to the last move time
            lastMove = LocalDateTime.now();
            // Provided that the enemy is not dead
            if (!allCrewDead() && !allCannonsDestroyed()) {
                // Decide what to do
                int move = rnd.nextInt(3);
                // Select a random Utility to repair
                if (move == 0) {
                    return repairUtility();
                } 
                // Select a random Utility to shoot at
                else if (move == 1) {
                    return shootEnemy(opponent);
                }
                // Select a cannon to repair
                else {
                    return setupCannon();
                }
            }
        }
        // If the enemy is dead, then it won't be doing anything
        return "";
    }

    
    /** 
     * Finds an idle crew member from the ArrayList with crew members
     * @return Crew
     */
    public Crew selectIdleCrew() {
    	// Set up an ArrayList local variable for all idle crew
        ArrayList<Crew> idleCrew = new ArrayList<>();
        
        // Cycle through all crew members, and if they are idle and not dead, add them to this ArrayList
        for (Crew crewMem : this.getAllCrew().values()) {
            if (crewMem.isIdle() && !crewMem.isDead()) {
                idleCrew.add(crewMem);
            }
        }
        // If there are no idle crew, then all of the crew are busy
        if (idleCrew.size() == 0) {
            return null;
        }
        // If there are idle crew, then a random selection from these will be selected to undertake the next task
        return idleCrew.get(rnd.nextInt(idleCrew.size()));
    }

    
    /** 
     * Selects a random cannon which is idle to be used
     * 
     * @param isReady What state the cannon should be in to shoot
     * @return Cannon
     */
    public Cannon selectCannon(boolean isReady) {
    	// Create an ArrayList of idle cannons
        ArrayList<Cannon> idleCannons = new ArrayList<>();
        // Cycle through every cannon, and the ones which are idle, not dead but ready should be added to the ArrayList as it is ready to be used
        for (Cannon cannon : this.getAllCannon().values()) {
            if (cannon.isIdle() && !cannon.isDead() && cannon.isReady() == isReady) {
                idleCannons.add(cannon);
            }
        }
        // If there are no idle cannons, then they are all busy
        if (idleCannons.size() == 0) {
            return null;
        }
        // Return a random idle cannon which is set up and is not dead
        return idleCannons.get(rnd.nextInt(idleCannons.size()));
    }

    
    /** 
     * Checks to see if a cannon requires setting up by going through the ArrayList of Cannons which require setting up
     * @return Cannon
     */
    // Select a cannon that needs setup
    public Cannon needsSetup() {
    	// Create an ArrayList of Cannons which need setting up
        ArrayList<Cannon> arrNeedsSetup = new ArrayList<>();
        // Cycle through every cannon, if it is not dead, and is not set up or being operated, then add this to the ArrayList
        for (Cannon cannon : this.getAllCannon().values()) {
            if (!cannon.isDead() && !cannon.isSetUp() && !isBeingOperated(cannon.getKey())) {
                arrNeedsSetup.add(cannon);
            }
        }
        // If there are none requiring setup, then do not return these
        if (arrNeedsSetup.size() <= 0) {
            return null;
        }
        // Return a random cannon which requires setting up
        return arrNeedsSetup.get(rnd.nextInt(arrNeedsSetup.size()));
    }
    
    
    /** 
     * Goes through an ArrayList to determine which utilities of the enemy reqire repairing
     * @return Utility
     */
    // Select an item that needs repair
    public Utility needsRepair() {
    	// Create an ArrayList of Utilities which require repairing
        ArrayList<Utility> ArrNeedsRepair = new ArrayList<>();
        // Cycle through all crew members, if they are damaged but not dead, then add them to the ArrayList
        for (Crew crewMem : this.getAllCrew().values()) {
            if (crewMem.isDamaged() &&  !crewMem.isDead()) {
                ArrNeedsRepair.add(crewMem); 
            }
        }
        // Cycle through all cannons, if they are damaged but not dead, then add them to the ArrayList
        for (Cannon cannon : this.getAllCannon().values()) {
            if (cannon.isDamaged() && !cannon.isDead()) {
                ArrNeedsRepair.add(cannon);
            }
        }
        // If nothing needs repairing, then return nothing
        if (ArrNeedsRepair.size() <= 0) {
            return null;
        }

        // Select a random from the Utilities which do need repairing
        return ArrNeedsRepair.get(rnd.nextInt(ArrNeedsRepair.size()));
    }

    
    /** 
     * Determine how long it has been since the last move
     * 
     * @return long
     */
    public long secsSinceLastMove() {
    	// This works out how long of a wait there is before the next enemy move
        return Duration.between(lastMove, LocalDateTime.now()).getSeconds();
    }

}