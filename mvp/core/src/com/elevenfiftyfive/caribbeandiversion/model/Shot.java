package com.elevenfiftyfive.caribbeandiversion.model;
import java.util.Random;

public class Shot extends Task {

    public Shot(int length, String targetKey) {
    	// Call the superclass constructor to deal with this, as all parameters are common methods
        super(length, targetKey);
    }

    
    /** 
     * Apply the shot
     * 
     * @param usingUtil The utility which is coordinating the shot
     * @param targetUtil The utility which is being shot at
     * @return Utility
     */
    @Override
    // Changes the TargetUtil using the UsingUtil
    public Utility apply(Utility usingUtil, Utility targetUtil) {
        int damage =  new Random().nextInt(30);
        damage = usingUtil.getQuality() + damage;
        targetUtil.setHealth(targetUtil.getHealth() - damage);

        if (targetUtil.getHealth() <0) {
        	// If the damage done to the Utility sends it to < 0 then round it back to 0
            targetUtil.setHealth(0); 
        }
        
        usingUtil.toggleActive();
        
        return targetUtil;
        
    }

}