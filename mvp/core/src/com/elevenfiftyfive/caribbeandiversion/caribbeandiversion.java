package com.elevenfiftyfive.caribbeandiversion;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.elevenfiftyfive.caribbeandiversion.states.GameStateManager;
import com.elevenfiftyfive.caribbeandiversion.states.MenuState;


public class caribbeandiversion extends ApplicationAdapter {
	// Set the size of the window
	public static final int WIDTH = 1280;
	public static final int HEIGHT = 720;
	// Give the window a title which can then be used in Application Management Programs and be displayed atop of the window
	public static final String TITLE = "Caribbean Diversion";
	// Make a GameStateManager which manages the current active states in the game in a stack-like manner
	private GameStateManager gsm;
	private SpriteBatch batch;

	@Override
	public void create() {
		// Create the instances of the SpriteBatch and GameStateManager
		batch = new SpriteBatch();
		gsm = new GameStateManager();
		// Push a MenuState to the GameStateManager, which therefore will peek at the top of the stack and begin to work with this state
		gsm.push(new MenuState(gsm));
		// This will set the background colour
		Gdx.gl.glClearColor(1, 0, 0, 1);
	}

	@Override
	public void render() {
		// This will clear any preset background colours
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		// This will set the frequency of the update, thus the frame rate, which is determined by the Gdx.graphics module
		gsm.update(Gdx.graphics.getDeltaTime());
		// Render the batch.
		gsm.render(batch);
	}

}
