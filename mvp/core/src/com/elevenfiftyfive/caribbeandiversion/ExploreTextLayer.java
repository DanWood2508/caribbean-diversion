// returns of layer labels 
package com.elevenfiftyfive.caribbeandiversion;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

// Labels for each aspect of the game
public class ExploreTextLayer {
    FreeTypeFontGenerator generator;
    FreeTypeFontParameter parameter;
    BitmapFont font;
    Label.LabelStyle lbStyle;

    int topScore;

    // Update only what has changed
    public ExploreTextLayer() {
        super();
        // Find out the top score using the method getTopScore()
        this.topScore = getTopScore();

        parameter = new FreeTypeFontParameter();

        // Set the font size and create a generator using font "regfont.ttf" in the Fonts file in this project.
        parameter.size = 25;
        parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
        generator = new FreeTypeFontGenerator(Gdx.files.internal("Fonts/regfont.ttf"));

        // Generate the font and create a label, apply the style to this and use this as a variable to apply to labels
        font = generator.generateFont(parameter);
        lbStyle = new Label.LabelStyle();
        lbStyle.font = font;

        lbStyle.fontColor = Color.BLACK;
        // The generator is no longer needed, dispose of this now to save resources
        generator.dispose();

    }

    
    /** 
     * @param newTopScore The new Top Score to be compared and potentially added
     */
    public static void updateTopScore(int newTopScore) {
    	// If the current top score is higher than the previous high score, then this will be replaced
        if (newTopScore > getTopScore()) {
            FileWriter myWriter;
            // Try and open the High Score file to change this, if not, output the exception so that this can be seen by the developer
            try {
                myWriter = new FileWriter("Caribbean Diversion - High Score.txt");
                // Try and write the new top score to the file, if this fails, throw an exception so that this can be seen by the developer
                try {
                    myWriter.write(Integer.toString(newTopScore));
                    myWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    
    /** 
     * Generates the Explore Labels to add to the Group
     * 
     * @param resourceRating The amount of resources the the player has
     * @param moneyRating The amount of money the player has
     * @param scoreRating The score of the player
     * @param x The X Position of the Panel
     * @param y The Y Position of the Panel
     * @return Group
     */
    public Group exploreInfo(int resourceRating, int moneyRating, int scoreRating, int x, int y) {
    	// Group all of the information into a panel and position these relatively
        Group panel = new Group();
        // Create a label with uniform spacing to ensure the utility values line up perfectly, place these in one column but at differing heights
        // Apply the defauly lbStyle to this, to ensure if is the correct font, colour and size.
        Label resources = new Label("Supplies:   " + String.valueOf(resourceRating), lbStyle);
        resources.setPosition(x + 10, y);

        Label money = new Label("Money:      " + String.valueOf(moneyRating), lbStyle);
        money.setPosition(x + 10, y - 75);

        Label score = new Label("Score:      " + String.valueOf(scoreRating), lbStyle);
        score.setPosition(x + 10, y - 150);

        Label lblTopScore = new Label("Top Score:" + String.valueOf(this.topScore),lbStyle);
        lblTopScore.setPosition(x + 120, y - 180);
        
        // Add the labels as actors to the group, so that the group can act collectively with them
        panel.addActor(resources);
        panel.addActor(money);
        panel.addActor(score);
        panel.addActor(lblTopScore);
        return panel;

    }

    
    /** 
     * @return int
     */
    public static int getTopScore() {
    	// Try and open the file which stores the local high score, if this is not possible the exception message will be outputted
        try {
            BufferedReader buffer;
            buffer = new BufferedReader(
                    new FileReader("Caribbean Diversion - High Score.txt"));
            // Try and parse the integer in the file on the first line, this can then be stored as the current top score within this thread
            try {
                int result = Integer.parseInt(buffer.readLine());
                buffer.close();

                return result;

            } catch (NumberFormatException | IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // If there is nothing in the file, then there is no current high score and thus 0 will be returned.
        return 0;
    }

    
    /** 
     * Generates the Explore Panel
     * 
     * @param resources The amount of resources the player has
     * @param money The amount of money the player has
     * @param score The score the player has
     * @return Group
     */
    public Group CreateExplorePanel(int resources, int money, int score) {
    	// Group the panel together so that this can all be moved together
        Group panel = new Group();

        int x = 32;
        int y = 147;
        // Add the other group as an actor to this panel, this nested panel structure means that all elements of the inner panel can be moved easily
        panel.addActor(exploreInfo(resources, money, score, x, y));

        return panel;
    }
    

}