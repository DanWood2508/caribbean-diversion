// returns of layer labels 
package com.elevenfiftyfive.caribbeandiversion;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

// Labels for each aspect of the game
public class BattleUtilityTextLayer {
    FreeTypeFontGenerator generator;
    FreeTypeFontParameter parameter;
    BitmapFont font;
    Label.LabelStyle lbStyle;

    int topScore;

    // Update only what has changed
    public BattleUtilityTextLayer() {
        super();

        // Store the top score by calling the method getTopScore() which is within this class
        this.topScore = getTopScore();

        parameter = new FreeTypeFontParameter();

        parameter.size = 1;
        // Get a Font Generator and then use regfont.ttf from the Fonts file (this is the only font we downloaded)
        parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
        generator = new FreeTypeFontGenerator(Gdx.files.internal("Fonts/regfont.ttf"));

        font = generator.generateFont(parameter);
        // Add the styling to the Label as it is created
        lbStyle = new Label.LabelStyle();
        // Set the font attribute of the style to the font we decided
        lbStyle.font = font;
        lbStyle.fontColor = Color.BLACK;
        // The generator is no longer needed, therefore it can be disposed of here
        generator.dispose();

    }

    
    /** 
     * Sets a new top score if the parameter is higher than the existing high score
     * @param newTopScore The integer score gained from the game
     */
    public static void updateTopScore(int newTopScore) {
    	// If the top score has changed, in that the new score is larger than the current top score
        if (newTopScore > getTopScore()) {
            FileWriter myWriter;
            // Try to open the file "heightScore"
            try {
                myWriter = new FileWriter("core/src/com/elevenfiftyfive/caribbeandiversion/model/heightScore");
                // Try and write the new score to this file
                try {
                    myWriter.write(Integer.toString(newTopScore));
                    myWriter.close();
                // If there are any issues, an exception can be thrown
                } catch (IOException e) {
                    e.printStackTrace();
                }
             // If there are any issues, an exception can be thrown
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    
    /** 
     * Generates the explore information panel text for the lower left panel on the battle screen
     * @param resourceRating The amount of resources currently possessed
     * @param moneyRating The amount of money currently possessed
     * @param scoreRating The score currently
     * @param x The x coordinate to draw the group
     * @param y The y coordinate to draw the group
     * @return Group
     */
    public Group exploreInfo(int resourceRating, int moneyRating, int scoreRating, int x, int y) {
    	// Create a new group which will contain all of the labels, which have relative positions inside this group
        Group panel = new Group();

        // Create the label, ensure there is consistent spacing in the first string so that scores are aligned, apply lbStyle to this too
        Label resources = new Label("Supplies: " + String.valueOf(resourceRating), lbStyle);
        // Set the position of the label, all in the same column so same x, but y varies based on order 
        resources.setPosition(x + 10, y);

        Label money = new Label("Money:      " + String.valueOf(moneyRating), lbStyle);
        money.setPosition(x + 10, y - 75);

        Label score = new Label("Score:    " + String.valueOf(scoreRating), lbStyle);
        score.setPosition(x + 10, y - 150);

        Label lblTopScore = new Label("Top Score:" + String.valueOf(this.topScore),lbStyle);
        lblTopScore.setPosition(x + 120, y - 180);

        // Add the labels to the Group so that they can be drawn when the group is drawn
        panel.addActor(resources);
        panel.addActor(money);
        panel.addActor(score);
        panel.addActor(lblTopScore);
        return panel;

    }

    
    /** 
     * Returns the integer top score for output
     * @return int
     */
    public static int getTopScore() {
        try {
        	// This will attempt to load the file itself which can then be read through below
            BufferedReader buffer;
            buffer = new BufferedReader(
                    new FileReader("core/src/com/elevenfiftyfive/caribbeandiversion/model/heightScore"));
            // Try to parse the integer on the top line, which is the current highest score
            try {
            	// Store the integer in the file and then close the buffer as this is no longer needed
                int result = Integer.parseInt(buffer.readLine());
                buffer.close();

                return result;
                
            // If there is an issue with parsing the integer, then this exception will be outputted to make the developer aware
            } catch (NumberFormatException | IOException e) {
                e.printStackTrace();
            }
        
        // If the file cannot be found, this will be outputted so that the developer is aware of the issue
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return 0;
    }

    
    /** 
     * Creates the group which holds the explore details, grouped to ease mass movements  / operations
     * 
     * @param resources The current amount of resources
     * @param money The current amount of money
     * @param score The current score
     * @return Group
     */
    public Group CreateExplorePanel(int resources, int money, int score) {
    	
    	// This makes a Group which invitably holds the other group, this is so that the group in its entirity can be moved
        Group panel = new Group();

        int x = 32;
        int y = 147;
        // Add the Group returned by exploreInfo to this group, which is then returned.
        panel.addActor(exploreInfo(resources, money, score, x, y));

        return panel;
    }

}