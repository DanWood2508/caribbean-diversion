package com.elevenfiftyfive.caribbeandiversion;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class BattleAudio implements ApplicationListener{
	// This is the background music that plays throughout the battle
	Music backgroundAudio;
	
	public BattleAudio() {
		// Call the create method to create the sound itself
		create();
	}
	
	@Override
	public void create() {
		// Create an instance of the audio, this should be done using the LibGDX importer
		backgroundAudio = Gdx.audio.newMusic(Gdx.files.internal("battleBackground.mp3"));
		// If the battle lasts longer than the track, then the track will loop
		backgroundAudio.setLooping(true);
		// This sets the volume to half of that of the computer and thus stops overpowering audio
		backgroundAudio.setVolume(0.5f);
		// This gets the background audio to play
		backgroundAudio.play();
	}

	
	/** 
	 * Resize the Window
	 * 
	 * @param width Width of the window
	 * @param height Height of the Window
	 */
	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void render() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		// Stop the audio playing and remove this from the LibGDX Buffer
		backgroundAudio.dispose();
	}

}
