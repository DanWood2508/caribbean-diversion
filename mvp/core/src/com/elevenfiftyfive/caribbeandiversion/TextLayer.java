package com.elevenfiftyfive.caribbeandiversion;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.elevenfiftyfive.caribbeandiversion.model.Cannon;
import com.elevenfiftyfive.caribbeandiversion.model.Crew;
import com.elevenfiftyfive.caribbeandiversion.model.Repair;
import com.elevenfiftyfive.caribbeandiversion.model.Shot;

// Labels for each aspect of the game
public class TextLayer {
    Group crew; // labels
    Group cannons; // labels
    Group Ship;
    FreeTypeFontGenerator generator;
    FreeTypeFontParameter parameter;
    BitmapFont font;
    Label.LabelStyle lbStyle;

    // Update only what has changed
    public TextLayer() {
        super();
        // Configure the font and its characteristics such as its size, character set and the font it will be using, as seen in the Font file
        parameter = new FreeTypeFontParameter();
        parameter.size = 10;
        parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
        generator = new FreeTypeFontGenerator(Gdx.files.internal("Fonts/regfont.ttf"));

        // The font will then be generated based upon what is given previously, a label style will also be formulated based upon these details
        font = generator.generateFont(parameter);
        lbStyle = new Label.LabelStyle();
        lbStyle.font = font;
        lbStyle.fontColor = Color.BLACK;
        
        // The generator is not needed past this point, so can be disposed of to reduce application memory usage.
        generator.dispose();

    }

    
    /** 
     * Generate the Label to then be added to a group and drawn
     * 
     * @param message The message to be added
     * @return Label
     */
    public Label getMessageLabel(String message) {
    	// This will return the message label by creating a new label and assigning all of the values to it
        Label.LabelStyle mLabelStyle = new Label.LabelStyle();
        mLabelStyle.font = font;
        mLabelStyle.fontColor = Color.BLACK;
        Label messageLabel = new Label(message, mLabelStyle);
        messageLabel.setFontScale(3);
        return messageLabel;
    }

    
    /** 
     * Generate and format the information about the canon
     * 
     * @param cannon The instance of the Cannon
     * @param x The X Position of the Panel
     * @param y The Y Position of the Panel
     * @return Group
     */
    public Group cannonInfo(Cannon cannon, int x, int y) {
    	// Create a group to hold the details about the cannon, this is to contain them
        Group panel = new Group();
        // This will declare the types of local (function) variables currentTask and currentVictim
        Label currentTask;
        Label currentVictim;
        
        // Checks to see if the cannon passed as a parameter is completing a task, and if so if the task is a Shot
        if (cannon.getCurrent() != null && cannon.getCurrent() instanceof Shot) {
        	// Assumes that the cannon is attacking, and counts down the seconds remaining, the victim is also assumed to be present
            try {
                currentTask = new Label("A : " + String.valueOf(cannon.getCurrent().getSecsRemaining()), lbStyle);
                currentVictim = new Label(String.valueOf(cannon.getCurrent().getTargetKey()), lbStyle);
            // If there is an exception (expecting a null pointer exception) then the cannon is not doing anything and this is Idle, this is styled as required
            } catch (Exception e) {
                currentTask = new Label("I", lbStyle);
                
                // Set the font colour to green
                Label.LabelStyle greenLb = new Label.LabelStyle();
                greenLb.font = font;

                // Show that the cannon is ready to attack
                greenLb.fontColor = Color.GREEN;
                currentVictim = new Label("Ready", greenLb);
            }
        } else {

            currentTask = new Label("I", lbStyle);
            // MAKE the current VICTIM READY if the cannon is ready to be shot or not loaded, if not
            if (cannon.isReady()) {
            	// Set the font to green to show that the victim cannon is ready
                Label.LabelStyle greenLb = new Label.LabelStyle();
                greenLb.font = font;
                greenLb.fontColor = Color.GREEN;
                currentVictim = new Label("Ready", greenLb);

            } else if (cannon.isDamaged()) {
                // If the cannon is damaged, then the font will be changed to red and will show that it requires repair
                Label.LabelStyle Red = new Label.LabelStyle();
                Red.font = font;

                Red.fontColor = Color.RED;

                currentVictim = new Label("Repair", Red);

            } else {
            	// If the cannon is not damaged but is also not ready for attack, then it needs to be setup, show this in yellow
                Label.LabelStyle yellow = new Label.LabelStyle();
                yellow.font = font;

                yellow.fontColor = Color.YELLOW;
                currentVictim = new Label("Setup", yellow);
            } 

        }
        // Position the two fields generated above in the spaces in the graphics
        currentVictim.setPosition(x + 25, y - 18);
        panel.addActor(currentVictim);

        return panel;

    }

    
    /** 
     * Generate and format the Crew information
     * 
     * @param crew The instance of Crew
     * @param x The X Position of the Panel
     * @param y The Y Position of the Panel
     * @return Group
     */
    public Group crewInfo(Crew crew, int x, int y) {
    	// Contain all of the crew information in a group
        Group panel = new Group();
        // All that needs to be shown is the name of the crew member which is being used in any task
        Label currentCrew;
        // If the crew is doing nothing, then nothing will be outputted but a blank space, this still needs to be defined if this changes
        if (crew.getCurrent() == null) {
            currentCrew = new Label("", lbStyle);
        }
        // if the current crew member is being repaired or is repairing
        else if (crew.getCurrent() instanceof Repair) {
        	// Assume that the crew member kis repairing somebody, if not then a null pointer exception is thrown and it is assumed that the crew member is idle
            try {
                currentCrew = new Label(crew.getCurrent().getTargetKey(), lbStyle);
            } catch (Exception e) {
                currentCrew = new Label("", lbStyle);
            }
        }
        // If the crew member doing something, but not repairing, it must be attacking, therefore get the target key and output this
        else {
            try {
                currentCrew = new Label(crew.getCurrent().getTargetKey(), lbStyle);
            }
            // Again, if the current crew member is not attacking, then just output an empty space as they are idle
            catch (Exception e) {
                currentCrew = new Label("", lbStyle);
            }

        }
        // Set the position and add these actors to the group
        currentCrew.setPosition(x + 25, y - 18);
        panel.addActor(currentCrew);
        panel.setPosition(x, y);
        return panel;
    }

    
    /** 
     * Create the Group to hold the Crew
     * 
     * @param crewMem The Crew Members to add details for
     * @return Group
     */
    public Group CreateCrewPanel(HashMap<String, Crew> crewMem) {
    	// Contain the group in a label so that they can be controlled easily
        Group panel = new Group();
        // Position the group on the screen
        int x = 32;
        int y = 147;
        // Call the crewInfo method for every crew member and add the group this produces into one collective group of groups
        for (Crew crew : crewMem.values()) {
            panel.addActor(crewInfo(crew, x, y));
            // Move each group down by -23 places
            y -= 23;
        }

        return panel;
    }
    
    
    /** 
     * Generate the information about the player for the Battle Utility Pane
     * 
     * @param resourceRating The resources the player has
     * @param moneyRating The money the player has
     * @param scoreRating The current score of the player
     * @param x The X Position of the Panel
     * @param y The Y Panel of the Panel
     * @return Group
     */
    public Group exploreInfo(int resourceRating, int moneyRating, int scoreRating, int x, int y) {
    	// Create a group to contain the utilities
        Group panel = new Group();
        
        // Add the additional spacing to the label so that the values are in line, add the default styling and arrange in a column
        Label resources = new Label("Supplies:   " + String.valueOf(resourceRating), lbStyle);
        resources.setPosition(x + 10, y);

        Label money = new Label("Money:      " + String.valueOf(moneyRating), lbStyle);
        money.setPosition(x + 10, y - 38);

        Label score = new Label("Score:      " + String.valueOf(scoreRating), lbStyle);
        score.setPosition(x + 10, y - 75);
        
        // Add these as actors to the group so that this can then be drawn
        panel.addActor(resources);
        panel.addActor(money);
        panel.addActor(score);
        return panel;

    }

    
    /** 
     * Create the Cannon Panel from a HashMap of all Cannons
     * 
     * @param cannons A HashMap of all Cannons to generate and format details for
     * @return Group
     */
    public Group CreateCannonPanel(HashMap<String, Cannon> cannons) {
    	// This createas a group to hold the groups of cannons
        Group panel = new Group();

        // Set the position of the cannon panel on the screen
        int x = 75;
        int y = 387;
        // For every cannon in the game then add the group to this group to control all groups together
        for (Cannon cannon : cannons.values()) {
            panel.addActor(cannonInfo(cannon, x, y));
            // Decrement the y position by 46 to seperate the groups for the panel
            y -= 46;
        }

        return panel;
    }
    
    
    /** 
     * Generate the Utility Panel for the bottom left of the Battle Screen
     * 
     * @param resources The resources the player has
     * @param money The money the player has
     * @param score The current score of the player
     * @return Group
     */
    public Group CreateUtilityPanel(int resources, int money, int score) {
    	// Create a group to hold the group of utilities
    	Group panel = new Group();
    	// Draw the group on the screen
        int x = 42;
        int y = 95;
        // Add the group created by exploreInfo to this group
        panel.addActor(exploreInfo(resources, money, score, x, y));

        return panel;
    }

}