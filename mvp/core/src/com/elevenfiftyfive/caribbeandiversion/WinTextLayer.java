// returns of layer labels 
package com.elevenfiftyfive.caribbeandiversion;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

// Labels for each aspect of the game
public class WinTextLayer {
    FreeTypeFontGenerator generator;
    FreeTypeFontParameter parameter;
    BitmapFont font;
    Label.LabelStyle lbStyle;

    // Update only what has changed
    public WinTextLayer() {
        super();

        // Create the font and its attributes, such as size, character set and font itself
        parameter = new FreeTypeFontParameter();
        parameter.size = 50;
        parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
        generator = new FreeTypeFontGenerator(Gdx.files.internal("Fonts/regfont.ttf"));

        // Generate this font and apply it to a label style used on every label
        font = generator.generateFont(parameter);
        lbStyle = new Label.LabelStyle();
        lbStyle.font = font;
        lbStyle.fontColor = Color.BLACK;
        
        // The generator is no longer required, dispose of this immediately to save application memory
        generator.dispose();

    }

    
    /** 
     * Generate and Format the Explore Information
     * 
     * @param resourceRating The resources the player has
     * @param moneyRating The money the player has
     * @param scoreRating The current score of the player
     * @param x The X Position of the Panel
     * @param y The Y Position of the Panel
     * @return Group
     */
    public Group exploreInfo(int resourceRating, int moneyRating, int scoreRating, int x, int y) {
    	// Create a group to hold the information
        Group panel = new Group();

        // For each of the pieces of information passed as parameters, make a label and apply the default styling, then set the position
        Label resources = new Label(String.valueOf(resourceRating), lbStyle);
        resources.setPosition(x+10,y+8);

        Label money = new Label(String.valueOf(moneyRating), lbStyle);
        money.setPosition(x+10, y - 70);
        
        Label health = new Label(String.valueOf(scoreRating),lbStyle);
        health.setPosition(x+10, y - 150);
        
        Label message = new Label("Click anywhere to continue your battle...", lbStyle);
        message.setPosition(x-250, y-300);

        // Add these to the Group as actors
        panel.addActor(resources);
        panel.addActor(money);
        panel.addActor(health);
        panel.addActor(message);

        return panel;

    }

    
    /** 
     * Generate the Win Panel
     * 
     * @param resources The resources the player has
     * @param money The money the player has
     * @param score The current score of the player
     * @return Group
     */
    public Group CreateWinPanel(int resources, int money, int score) {
    	// Create a Group to hold these rows of information so only one item needs to be drawn
        Group panel = new Group();
        
        // Set the position and add the explore information to the panel
        int x = 32;
        int y = 147;
        panel.addActor(exploreInfo(resources, money, score, x, y));

        return panel;
    }

}
