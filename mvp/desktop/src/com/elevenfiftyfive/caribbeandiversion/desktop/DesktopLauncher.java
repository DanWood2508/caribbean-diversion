package com.elevenfiftyfive.caribbeandiversion.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.elevenfiftyfive.caribbeandiversion.caribbeandiversion;

public class DesktopLauncher {
	
	
	/** 
	 * Start the program
	 * 
	 * @param arg Default arguments to start the gamex
	 */
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = caribbeandiversion.WIDTH;
		config.height = caribbeandiversion.HEIGHT;
		config.title = caribbeandiversion.TITLE;	
		config.resizable = false;
		
		new LwjglApplication(new caribbeandiversion(), config);
	}
}