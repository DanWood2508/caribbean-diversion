package com.elevenfiftyfive.caribbeandiversion;

import java.util.HashMap;

import com.elevenfiftyfive.caribbeandiversion.model.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

@TestMethodOrder(OrderAnnotation.class)
public class ShipTest {

    static Ship ship;

    @BeforeAll
    public static void setup() {

        HashMap<String, Crew> crew = new HashMap<>();
        crew.put("dave", new Crew(1, "dave", 30, 0, 0, false));
        crew.put("john", new Crew(2, "john", 20, 0, 0, false));
        crew.put("james", new Crew(5, "james", 30, 0, 0, false));

        HashMap<String, Cannon> cannons = new HashMap<>();

        cannons.put("Left", new Cannon(3, "Left", 15, 0, 0, false));
        cannons.put("Right", new Cannon(3, "Right", 20, 0, 0, false));
        ship = new PlayerShip(cannons, crew, 10, 10, 10);

    }

    @Test
    @Order(1)
    public void getUtil() {
        // test get util return a cannon when we request the util named "Left"
        assertEquals(true, ship.getUtil("Left") instanceof Cannon);
        assertEquals(true, ship.getUtil("dave") instanceof Crew);
    }

    
    /** 
     * @throws Exception
     */
    @Test()
    @Order(2)
    public void setUpTask() throws Exception {

        // check if setting up a cannon works
        // test get util return a cannon when we request the util named "Left"
        // start a setup task
        ship.setSetupTask("Left", ship.getCrew("dave"));

        assertEquals("Left", ship.getCrew("dave").getCurrent().getTargetKey());
        assertEquals(false, ship.getCrew("dave").isIdle());

        long millSecsforSetup = ship.getCrew("dave").getCurrent().getSecsRemaining() * 1000;
        assertEquals(false, ship.getCrew("dave").getCurrent().isDone());
        System.out.print("waiting for task to complete lenght + " + millSecsforSetup);

        // wait for theship.update(); task to finish
        Thread.sleep(millSecsforSetup);
        // now complete the task
        ship.getCrew("dave").finishCurrent();

        assertEquals(true, ship.getCrew("dave").hasSetup("Left"));
    }

    @Test()
    @Order(3)
    public void updateShip() {
        ship.update();
        assertEquals(true, ship.getCannon("Left").isReady());
    }

    @Test()
    @Order(4)
    public void shoot() // check if the cannons can shoot after setup
    {
        Crew testTarget = new Crew(30, "empty trarget", 300, 0, 0, false);
        ship.setShootingTask("Left", "james", testTarget.getKey());

        assertEquals(true, ship.getCannon("Left").getCurrent() != null);
        assertEquals("james", ship.getCannon("Left").getShooterKey());

    }
}