package com.elevenfiftyfive.caribbeandiversion;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.elevenfiftyfive.caribbeandiversion.model.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

@TestMethodOrder(OrderAnnotation.class)
public class BattleTest {

    static Battle battle;
    private static ExecutorService service;

    @BeforeAll
    public static void setup() {
        HashMap<String, Crew> crew = new HashMap<>();
        crew.put("dew", new Crew(1, "dew", 100, 0, 0, false));
        crew.put("pen", new Crew(2, "pen", 100, 0, 0, false));
        crew.put("loc", new Crew(5, "loc", 100, 0, 0, false));

        HashMap<String, Cannon> cannons = new HashMap<>();

        cannons.put("Left", new Cannon(3, "Left", 100, 0, 0, false));
        cannons.put("Right", new Cannon(3, "Right", 100, 0, 0, false));

        battle = new Battle(3, new PlayerShip(cannons, crew, 10, 10, 10));
        // speed up the battle to make testing faster
        battle.toggleTesting();

        service = Executors.newFixedThreadPool(4);
        service.submit(battle);
    }

    
    /** 
     * @throws InterruptedException
     */
    @Test
    @Order(1)
    public void shootEnemy() throws InterruptedException {
        // select a random util from the list of enemy
        Utility enemyUtil = selectRandomEnemyUtil();
        // select a random util
        battle.setSetup("dew", "Left");
        Thread.sleep(1000); // wait for the setup to complete

        battle.shoot("Left", enemyUtil.getKey(), "dew");
        Thread.sleep(1000);
        battle.applyDamages();
        // Crew testTarget = new Crew(30,"empty trarget",300);
        boolean dam = battle.getEnemy().getUtil(enemyUtil.getKey()).isDamaged();
        assertEquals(true, dam);
        // assertEquals("james" ,ship.getCannon("Left").getShooterKey());
    }

    
    /** 
     * @return Utility
     */
    public Utility selectRandomEnemyUtil() {
        Random rnd = new Random();

        if (rnd.nextBoolean())
            return battle.getEnemy().getArrayListCannons()
                    .get(rnd.nextInt(battle.getEnemy().getArrayListCannons().size()));

        return battle.getEnemy().getArrayListCrew().get(rnd.nextInt(battle.getEnemy().getAllCrew().size()));
    }

}